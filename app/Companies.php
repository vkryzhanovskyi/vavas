<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Companies extends Model
{

    protected $table = 'companies';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        ''
    ];


}

