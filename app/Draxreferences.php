<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Draxreferences extends Model
{

    protected $table = 'draxreferences';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        ''
    ];


}

