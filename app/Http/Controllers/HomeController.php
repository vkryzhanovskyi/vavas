<?php

namespace App\Http\Controllers;

use App\User;
use App\Risersfallers;
use App\Rns;
use App\Candidates;
use App\Sectors;
use App\Companies;
use App\Careers;
use App\Education;
use App\Draxreferences;
use App\Codirectors;
use App\Filters;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    function __construct(){
        $this->middleware('auth', ['except' => ['index', 'login']]);
    }


    public function index() {
        return view('home/index');
    }

    public function login(Request $request)
    {
        // validate the info, create rules for the inputs
        $data = Input::all();
        $user = new User();
        $validate = $user->validateLogin($data);

        if (!is_bool($validate)) {
            return Redirect::to('/')
                ->withErrors($validate)
                ->withInput(Input::except('password'));
        }

        $user = User::where('user','=',$data['user'])->first();
        if(is_null($user)){
            return Redirect::to('/')
                ->withErrors(array('user' => 'user does not exist'))
                ->withInput(Input::except('password'));
        }
        // create user data for the authentication
        $userdata = array(
            'user'     => Input::get('user'),
            'password'  => Input::get('password')
        );
        // attempt to do the login
        if (Auth::attempt($userdata)) {
            DB::table('log_users')->insert(
                [
                    'user_id' => Auth::user()->id,
                    'ip' => $request->ip(),
                    'action' => 'login',
                    'user_agent' => $request->server('HTTP_USER_AGENT'),
                    'created_at' => new \DateTime ]
            );
            return Redirect::to('/dashboard');

        } else {
            return Redirect::to('/')
                ->withErrors(array('user' => 'Incorrect password'))
                ->withInput(Input::except('password'));

        }
    }

    public function logout(Request $request)
    {
        DB::table('log_users')->insert(
            [
                'user_id' => Auth::user()->id,
                'ip' => $request->ip(),
                'action' => 'logout',
                'user_agent' => $request->server('HTTP_USER_AGENT'),
                'created_at' => new \DateTime ]
        );

        Auth::logout();
        return Redirect::to('/');
    }

    public function dashboard(Request $request)
    {
        $page = $request->input('page');
        $risersfallers = Risersfallers::paginate(20);
        $rns = Rns::orderBy('RNS_Date', 'desc')->paginate(20);
        if(null === $page) {
            return view('home/dashboard', [
                'risersfallers' => $risersfallers,
                'rns' => $rns
            ]);

        } else if($request->input('rf')) {
            return response()
                ->json(['risersfallers' => $risersfallers]);
        } else if($request->input('rns')) {
            return response()
                ->json(['rns' => $rns]);
        }

    }

    public function search(Request $request)
    {
        $params = $request->all();
        $userId = Auth::user()->id;
        $filters = Filters::all()->groupBy('header');
        $locations = $filters['location'];
        $sectors = $filters['sector'];
        $filters['location'] = $locations->groupBy('parend_id');
        $filters['sector'] = $sectors->groupBy('parend_id');
        $userLastSearch = DB::table('searches')->where('user_id', '=', $userId)->first();
        if (null === $userLastSearch) {
            $checked = Filters::where('checked', '=', 1)->get();
            foreach($checked as $key => $filter) {
                $userLastSearch['filters'][$filter->header][] = $filter->value;
            }

            $this->editUserSearchFilters($userId, $userLastSearch);
            $userLastSearch = DB::table('searches')->where('user_id', '=', $userId)->first();
        } else if (!empty($request->except('page'))) {
            $this->editUserSearchFilters($userId, $request->except('page'));
            $userLastSearch = DB::table('searches')->where('user_id', '=', $userId)->first();
            $params['page'] = $request->input('page');
        } else if(!empty($request->except('page'))) {
            $this->editUserSearchFilters($userId, $request->except('page'));
            $userLastSearch = DB::table('searches')->where('user_id', '=', $userId)->first();
        }
        $search = json_decode( $userLastSearch->search, true );
        $params = $search;
        $candidates = new Candidates();
        $profiles = $candidates->getCandidatesList($search);
        DB::table('log_searches')->insert(
            [
                'user_id' => Auth::user()->id,
                'criteria' => json_encode($params),
                'count_results' => $profiles->count(),
                'created_at' => new \DateTime ]
        );
        return view('home/search', ['filters' => $filters, 'profiles' => $profiles, 'search' => $search]);

    }

    public function search1(Request $request)
    {
        $params = $request->all();
        $userId = Auth::user()->id;
        $search = DB::table('searches')->where('user_id', '=', $userId)->first();
        if(!empty($params)) {

            if(null !== $search) {
                if($request->input('page')) {
                    $search = DB::table('searches')->where('user_id', '=', $userId)->first();
                    $params = json_decode($search->search, true);
                    $params['page'] = $request->input('page');
                } else {
                    $this->editUserSearchFilters($userId, $params);
                    $search = DB::table('searches')->where('user_id', '=', $userId)->first();
                    $params = json_decode($search->search, true);
                }
            } else {
                $params['filters']['gender'][] = 'F';
                $params['filters']['gender'][] = 'M';
                $this->editUserSearchFilters($userId, $request->except('page'));
            }
        } else if(null !== $search) {
            $search = DB::table('searches')->where('user_id', '=', $userId)->first();
            $params = json_decode($search->search, true);
        } else if (empty($params) && (null == $search)) {
            $params['filters']['gender'][] = 'F';
            $params['filters']['gender'][] = 'M';
            $this->editUserSearchFilters($userId, $params);
        } else if (empty($params) && ($search !== null)) {
            $params = DB::table('searches')->where('user_id', '=', $userId)->first();
        }
        $candidates = new Candidates();
        $profiles = $candidates->getCandidatesList($params);
        return view('home/search', ['profiles' => $profiles, 'search' => $params]);
    }

    public function editUserSearchFilters($userId, $params)
    {
        DB::table('searches')->where('user_id', '=', $userId)->delete();
        DB::table('searches')->insert(
            ['user_id' => $userId, 'search' => json_encode($params)]
        );
    }

    public function profile($Candidate_ID)
    {
        $candidate = Candidates::where('Candidate_ID','=',$Candidate_ID)->first();
        if(is_null($candidate)){
            return $this->response(false, ['message' => 'Profile not found']);
        }
        $profile = $this->getProfile($candidate);
        DB::table('log_profiles')->insert(
            [
                'user_id' => Auth::user()->id,
                'profile_id' => $profile->Candidate_ID,
                'created_at' => new \DateTime ]
        );
        return view('home/profile', ['profile' => $profile]);
    }

    public function getProfile($candidate)
    {
        $candidate->sectors = Sectors::where('Candidate_ID', '=', $candidate->Candidate_ID)->get(array('Adv_Sector'));
        $candidate->age = date_diff(date_create($candidate->Candidate_DoB), date_create('today'))->y;
        $candidate->companies = Companies::where('Plc_ID', '=', $candidate->Candidate_Current_Plc_ID)->first();
        $candidate->careers = Careers::where('Candidate_ID', '=', $candidate->Candidate_ID)->orderBy('Career_End','desc')->get();
        $candidate->eductaiton = Education::where('Candidate_ID', '=', $candidate->Candidate_ID)->get();
        $candidate->draxreferences = Draxreferences::where('Candidate_ID', '=', $candidate->Candidate_ID)->get();
        $candidate->codirectors = Codirectors::where('Candidate_ID', '=', $candidate->Candidate_ID)->get();
        return $candidate;
    }


    public function update()
    {
        return view('home/update');
    }







}
