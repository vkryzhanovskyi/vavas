<?php

namespace App\Http\Controllers;

use App\Candidates;
use App\Careers;
use App\Companies;
use App\Risersfallers;
use App\Rns;
use App\Sectors;
use App\User;
use App\Education;
use App\Draxreferences;
use App\Codirectors;
use Illuminate\Support\Facades\Auth;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Support\Facades\Input;

class UserController extends Controller
{
    function __construct(){
        $this->middleware('jwt.auth', ['except' => ['login']]);
    }

    /**
     *  basePath="/",
     *  @SWG\Info(
     *         version="1.0.0",
     *         title="DRAX API",
     *         description=""
     *     ),
     * @SWG\Post(path="/api/login",
     *   tags={"Services"},
     *   summary="Perform login",
     *   description="Login and password to log in.",
     *   produces={"application/json"},
     *   consumes={"application/json"},
     *   @SWG\Parameter(
     *     in="body",
     *     name="login object",
     *     description="JSON Object which represents filter conditions",
     *     required=true,
     *
     *     @SWG\Schema(
     *         type="object",
     *         @SWG\Property(property="user", type="string", example=""),
     *         @SWG\Property(property="password", type="string", example="")
     *     )
     *   ),
     *   @SWG\Response(response="200", description="Rerurn Error or Object User")
     * )
     */
    public function login()
    {
        $data = Input::all();
        $user = new User();
        $validate = $user->validateLogin($data);
        if (!is_bool($validate)) {
            return $this->response(false, $validate);
        }

        $user = User::where('user','=',$data['user'])->first();
        if(is_null($user)){
            return $this->response(false, ['user' => 'user does not exist']);
        }

        $token = JWTAuth::attempt(['user' => $data['user'], 'password' => $data['password']]);
        if($token){
            Auth::attempt(['user' => $data['user'], 'password' => $data['password']]);
            $user = Auth::user();
            $user->accessToken = $token;
            return $this->response(true, $user);
        }

        return $this->response(false, ['password' => 'Incorrect password']);
    }

    /**
     * @SWG\GET(path="/api/logout",
     *   tags={"Services"},
     *   summary="Perform logout",
     *   description="logout User",
     *   produces={"application/json"},
     *   @SWG\Response(response="200", description="Logout User, redirect to Home")
     * )
     */
    public function logout()
    {
        if(JWTAuth::invalidate(JWTAuth::getToken())) {
            return $this->response(true);
        }
        return $this->response(false, 'token_invalid');
    }

    /**
     * @SWG\GET(path="/api/risersfallers",
     *   tags={"Services"},
     *   summary="GET Risers & Fallers",
     *   description="Risers & Fallers",
     *   produces={"application/json"},
     *   @SWG\Response(response="200", description="")
     * )
     */
    public function risersfallers()
    {
        $user = JWTAuth::parseToken()->authenticate();
        if(is_null($user)){
            return $this->response(false, ['message' => 'User not authorized']);
        }
        $risersfallers = Risersfallers::paginate(20);

        return $this->response(true, $risersfallers);

    }

    /**
     * @SWG\GET(path="/api/rns",
     *   tags={"Services"},
     *   summary="GET RNS Alerts",
     *   description="RNS Alerts",
     *   produces={"application/json"},
     *   @SWG\Response(response="200", description="")
     * )
     */
    public function rns()
    {
        $user = JWTAuth::parseToken()->authenticate();
        if(is_null($user)){
            return $this->response(false, ['message' => 'User not authorized']);
        }
        $rns = Rns::paginate(20);

        return $this->response(true, $rns);

    }

    /**
     * @SWG\GET(path="/api/search",
     *   tags={"Services"},
     *   summary="Get profiles",
     *   description="Get Candidates profiles",
     *   produces={"application/json"},
     *   @SWG\Parameter(
     *     in="body",
     *     name="Search params",
     *     description="filters[gender][]=M&
           filters[location][]=Newcastle-upon-Tyne&
            filters[sector][]=Industrial Engineering&
           filters[nee][]=NED&
           filters[ipo][]=Yes&
           filters[capacity][]=Yes",
     *    @SWG\Schema(
     *         type="string", example=""
     *     )
     *   ),
     *   @SWG\Response(response="200", description="")
     * )
     */

    public function search()
    {
        $data = Input::all();
        $candidates = new Candidates();
        $profiles = $candidates->getCandidatesList($data);
        return $this->response(true, $profiles);
    }

    /**
     * @SWG\GET(path="/api/profile/{Candidate_ID}",
     *   tags={"Services"},
     *   summary="Perform view candidate profile",
     *   description="view candidate profile",
     *   produces={"application/json"},
     *   consumes={"application/json"},
     *     @SWG\Parameter(
     *     in="path",
     *     type="string",
     *     name="Candidate_ID",
     *     description="Candidate_ID (100001)",
     *     required=true
     *   ),
     *   @SWG\Response(response="200", description="")
     * )
     */
    public function profile($Candidate_ID)
    {
        $candidate = Candidates::where('Candidate_ID','=',$Candidate_ID)->first();
        if(is_null($candidate)){
            return $this->response(false, ['message' => 'Profile not found']);
        }

        $profile = $this->getProfile($candidate);

        return $this->response(true, $profile);
    }

    public function getProfile($candidate)
    {
        $candidate->sectors = Sectors::where('Candidate_ID', '=', $candidate->Candidate_ID)->get(array('Adv_Sector'));
        $candidate->age = date_diff(date_create($candidate->Candidate_DoB), date_create('today'))->y;
        $candidate->companies = Companies::where('Plc_ID', '=', $candidate->Candidate_Current_Plc_ID)->first();
        $candidate->careers = Careers::where('Candidate_ID', '=', $candidate->Candidate_ID)->get();
        $candidate->eductaiton = Education::where('Candidate_ID', '=', $candidate->Candidate_ID)->get();
        $candidate->draxreferences = Draxreferences::where('Candidate_ID', '=', $candidate->Candidate_ID)->get();
        $candidate->codirectors = Codirectors::where('Candidate_ID', '=', $candidate->Candidate_ID)->get();
        return $candidate;
    }







}