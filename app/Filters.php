<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Filters extends Model
{

    protected $table = 'filters';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        ''
    ];


}

