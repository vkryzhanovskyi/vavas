<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Candidates extends Model
{

    protected $table = 'candidates';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        ''
    ];

    public function getCandidatesList($data)
    {
        $filters = (isset($data['filters']) && is_array($data['filters'])) ? $data['filters'] : [];
        $genders = (isset($filters['gender']) && is_array($filters['gender'])) ? $filters['gender'] : [];
        $locations = (isset($filters['location']) && is_array($filters['location'])) ? $filters['location'] : [];
        $sectors = (isset($filters['sector']) && is_array($filters['sector'])) ? $filters['sector'] : [];
        $careers = (isset($filters['nee']) && is_array($filters['nee'])) ? $filters['nee'] : [];
        $ipos = (isset($filters['ipo']) && is_array($filters['ipo'])) ? $filters['ipo'] : [];
        $capacities = (isset($filters['capacity']) && is_array($filters['capacity'])) ? $filters['capacity'] : [];

        $candidates = DB::table('candidates')
            ->leftJoin('sectors', 'candidates.Candidate_ID', '=', 'sectors.Candidate_ID')
            ->leftJoin('companies', 'candidates.Candidate_Current_Plc_ID', '=', 'companies.Plc_ID')
            ->where(function ($query) use ($genders) {
                foreach($genders as $gender) {
                    if ( $gender != "") {
                        $query->orWhere('candidates.Candidate_Gender', '=', $gender);
                    }
                }
            })
            ->where(function ($query) use ($locations) {
                foreach($locations as $location) {
                    if ( $location != "") {
                        $query->orWhere('candidates.Candidate_City', '=', $location);
                    }
                }
            })
            ->where(function ($query) use ($sectors) {
                foreach($sectors as $sector ) {
                    if ( $sector != "") {
                        $query->orWhere('sectors.Adv_Sector', '=', $sector);
                    }
                }
            })
            ->where(function ($query) use ($careers) {
                foreach($careers as $career) {
                    if ( $career != "") {
                        if($career == 'Chair') {
                            $query->where('candidates.Candidate_Chair', '=', 'Y');
                        }
                        if($career == 'Remuneration') {
                            $query->where('candidates.Candidate_Remuneration', '=', 'Y');
                        }
                        if($career == 'Nomination') {
                            $query->where('candidates.Candidate_Nomination', '=', 'Y');
                        }
                        if($career == 'Audit') {
                            $query->where('candidates.Candidate_Audit', '=', 'Y');
                        }
                        if($career == 'SID') {
                            $query->where('candidates.Candidate_SID', '=', 'Y');
                        }
                        if($career == 'NED') {
                            $query->where('candidates.Candidate_NED', '=', 'Y');
                        }
                        if($career == 'CSR') {
                            $query->where('candidates.Candidate_CSR', '=', 'Y');
                        }
                    }
                }
            })
            ->where(function ($query) use ($ipos) {
                foreach($ipos as $ipo) {
                    if ( $ipo != "") {
                        if($ipo == 'Yes') {
                            $query->orWhere('candidates.Candidate_Number_IPOs', '=', 'Y');
                        }
			if($ipo=='No'){
			    $query->orWhere('candidates.Candidate_Number_IPOs','=','');
			}
                    }
                }
            })
            ->where(function ($query) use ($capacities) {
                foreach($capacities as $capacity) {
                    if ( $capacity != "") {
                        if($capacity == 'Yes') {
                            $query->orWhere('candidates.Candidate_Capacity', '=', 'Y');
                        }
                        if($capacity == 'No') {
                            $query->orWhere('candidates.Candidate_Capacity', '=', '');
                        }
                    }
                }
            })
            ->groupBy('candidates.Candidate_ID')
            ->paginate(25);
        return $candidates;

    }
}

