<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Sectors extends Model
{

    protected $table = 'sectors';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        ''
    ];


}

