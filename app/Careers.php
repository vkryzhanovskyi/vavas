<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Careers extends Model
{

    protected $table = 'careers';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        ''
    ];


}

