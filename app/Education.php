<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Education extends Model
{

    protected $table = 'education';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        ''
    ];


}

