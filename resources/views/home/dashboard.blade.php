@extends('layouts.app')

@section('title', 'Dashboard')

@section('js')
    {!! Html::script('js/thirdParty/jquery-3.1.0.min.js') !!}
    {!! Html::script('js/thirdParty/countUp.js') !!}
    {!! Html::script('js/nav.js') !!}
    <!-- {!! Html::script('js/LLDB.js') !!}  -->
    {!! Html::script('js/mainSplash.js') !!}

@endsection

@section('css')
    {!! Html::style('css/style.css') !!}
    {!! Html::style('css/Header.css') !!}
    {!! Html::style('css/Footer.css') !!}
    {!! Html::style('css/mainSplash.css') !!}
@endsection

@section('content')

<script type="text/javascript" >

    var dashboard = {
        RFprocessing: false,
        RNSprocessing: false,
        RFnextPage : '<?php echo $risersfallers->nextPageUrl();  ?>',
        RNSnextPage : '<?php echo $rns->nextPageUrl();  ?>',
        init: function() {
            this.scrollRaserAndFallers();
            this.scrollRNS();
        },
        scrollRaserAndFallers: function() {
            $('#riseFall').scroll(function(e){
                if (dashboard.RFprocessing)
                    return false;

                if ($('#riseFall').scrollTop() >= ($('#riseFall').height() - $('#riseFall').height())*0.7){
                    dashboard.RFprocessing = true;
                    $.get(dashboard.RFnextPage + '&rf=true', function(data){
                        $.each(data.risersfallers.data, function(i,v){
                            $('#riseFall h3').last().after('<h3>' + v.text + '</h3>');
                        });
                        if(!data.risersfallers.next_page_url) {
                            dashboard.RFnextPage = null;
                            dashboard.RFprocessing = true;
                        } else {
                            dashboard.RFnextPage = data.risersfallers.next_page_url;
                            dashboard.RFprocessing = false;
                        }
                    });
                }
            });
        },
        scrollRNS: function() {
            $('#RNS').scroll(function(e){
                if (dashboard.RNSprocessing)
                    return false;

                if ($('#RNS').scrollTop() >= ($('#RNS').height() - $('#RNS').height())*0.7){
                    dashboard.RNSprocessing = true;
                    $.get(dashboard.RNSnextPage + '&rns=true', function(data){
                        $.each(data.rns.data, function(i,v){
                            $('#RNS p').last().after('<h3><a href="http://investegate.co.uk/article.aspx?id='+v.RNS_Item_ID+'" target="_blank">' + v.RNS_Headline + '</a></h3><p>' + v.RNS_Body.substring(0,600)+'...' + '</p>');
                        });
                        if(!data.rns.next_page_url) {
                            dashboard.RNSnextPage = null;
                            dashboard.RNSprocessing = true;
                        } else {
                            dashboard.RNSnextPage = data.rns.next_page_url;
                            console.log(dashboard.RNSnextPage);
                            dashboard.RNSprocessing = false;
                        }
                    });
                }
            });
        }
    }

    $(document).ready(function(){
       dashboard.init();
    });
</script>

<body onload="mainSplash.onLoad()">

    <div id="wrapper">
        <div class="page_common_header" id="Header">
            <div style="height:66px"></div>
            <div class="primarytabs"><!--
                --><div class="active" onclick="location.href = '/dashboard';">Dashboard</div><!--
                --><div onclick="location.href = '/search';">Search</div><!--
                --><div onclick="location.href = '/logout';">Log Out</div><!--
            --></div>
            </div>
        <div id="PageOuter">
            <div id="PageInner">
                <div id="DashTitle"><h1 style="text-align:center" class="mainHeading">Dashboard</h1></div>
                <div id="riseFall" style="overflow: scroll">
                    <h2 class="subHeading">Numis 100: Risers & Fallers</h2>
                    <?php foreach($risersfallers as $risersfaller) { ?>
                            <h3><?php echo $risersfaller->text; ?></h3>
                    <?php } ?>
                </div>
                <div id="RNS">
                    <h2 class="subHeading">RNS Alerts
                        <?php foreach($rns as $value) { ?></h2>
                        <h3><a href=<?php echo "http://www.investegate.co.uk/article.aspx?id=" . $value->RNS_Item_ID; ?> target="_blank"><?php echo $value->RNS_Headline; ?></a></h3>
                        <p><?php echo substr($value->RNS_Body,0,600) . "..."; ?></p>
                        <?php } ?>
                </div>
                <div id="Methodology">
                    <h2 class="subHeading-left">Methodology</h2>
                        <p>Numis 100 is a digital platform powered by a drxDATA proprietary database that tracks in real-time UK PLC Non-Executive Directors. We evaluate these individuals by the performance of their PLCs during their tenure, using the measures of EPS & 3 year revenue CAGR. This allows us to isolate those with a proven track record of delivering shareholder value.<br>We have then considered the background of each of these individuals in order to isolate their Non-Executive experience, their IPO experience and their capacity to consider new roles. These factors combine to allow you to isolate the ideal candidates to form a PLC board.</p>
                </div>
            </div>
        </div>
        <div id="Footer" class="page_common_footer"><span>&copy; DRAX 2016</span></div>
    </div>
</body>
@endsection
