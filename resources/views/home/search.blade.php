@extends('layouts.app')

@section('title', 'Search')

@section('js')
    {!! Html::script('js/thirdParty/jquery-3.1.0.min.js') !!}
    {!! Html::script('js/nav.js') !!}
    {!! Html::script('js/code.js') !!}
    {!! Html::script('js/FC.js') !!}
    {!! Html::script('js/LLDB.js') !!}
    {!! Html::script('js/Fields.js') !!}
    {{--{!! Html::script('js/SearchCommon.js') !!}--}}
    {{--{!! Html::script('js/search.js') !!}--}}
    {!! Html::script('js/jquery.main.js') !!}
@endsection

@section('css')
    {!! Html::style('css/style.css') !!}
    {!! Html::style('css/Header.css') !!}
    {!! Html::style('css/Footer.css') !!}
    {!! Html::style('css/search.css') !!}
    {!! Html::style('css/jcf.css') !!}
    {!! Html::style('css/icomoon.css') !!}
@endsection
<style type="text/css">
    input[type=checkbox] {
        margin: 0 auto!important;
        float: left;
    }
    #filtercontent div :nth-child(1) {
        margin-top: 0!important;
    }

</style>

@section('content')
    <script type="text/javascript">
        var search = {
            criterias : ['gender', 'location', 'sector', 'nee', 'ipo', 'capacity'],
            init: function() {
                this.initFields();
                return false;
            },
            initFields: function() {

            },
            selectAllGender: function() {
                $(search.criterias).each(function( i, criteria){
                    $(':checkbox.sub.' + criteria).change(function(){
                        $parent = $(this).closest('.sub_parent');
                        if($parent.find('input[type=checkbox]:first').is(':checked')) {
                            $parent.find('input[type=checkbox]:first').prop("checked", false);
                        } else {
                            if ($parent.find('.sub.' + criteria).length == $parent.find('.sub.' + criteria + ':checked').length) {
                                $parent.find('input[type=checkbox]:first').prop("checked", true);
                            }
                        }
                        $(this).find('.' + criteria).not('.sub').remove();
                    });

                    $(':checkbox.' + criteria).change(function(){

                        if(!$(this).hasClass('sub')) {
                            $parent = $(this).closest('.sub_parent');
                            if($parent.find('input[type=checkbox]:first').is(':checked')) {
                                $($parent.find('input[type=checkbox]').not(':first')).each(function(i, v) {
                                    $(v).prop("checked", true);
                                });
                            } else {
                                $($parent.find('input[type=checkbox]').not(':first')).each(function(i, v) {
                                    $(v).prop("checked", false);
                                });
                            }
                        }

                        if ($(':checkbox.' + criteria + ':checked').length == $(':checkbox.' + criteria).length) {
                            $(':checkbox.' + criteria + '_parent').prop("checked", true);
                        }

                    });

                    $(':checkbox.' + criteria).on('click', function(event) {
                        $(':checkbox.' + criteria).each(function(i, v) {
                            if(!$(v).is(':checked')) {
                                $(':checkbox.' + criteria + '_parent').prop("checked", false);
                                return false;
                            }
                        });
                    });
                    $(':checkbox.'+ criteria +'_parent').on('click', function(event) {
                        if(this.checked) {
                            $(':checkbox.' + criteria).each(function() {
                                this.checked = true;
                            });
                        } else {
                            $(':checkbox.' + criteria).each(function() {
                                this.checked = false;
                            });
                        }
                    });
                });
            }
        }

        $(document).ready(function(){
//            search.init();
            search.selectAllGender();
        });
    </script>
    <body >
    <div id="wrapper">
        <div class="page_common_header" id="Header">
            <div style="height:66px"></div>
            <div class="primarytabs"><!--
                --><div onclick="location.href = '/dashboard';">Dashboard</div><!--
                --><div class="active" onclick="location.href = '/search';">Search</div><!--
                --><div onclick="location.href = '/logout';">Log Out</div><!--
            --></div>
        </div>
        <div id="PageOuter">
            <div id="PageInner" class="pageinner">
        
                <div id="xxFiltersOuter" class="filtersouter">
                    <div class="filtertabline">
                        <div class="filtertab"><span>FILTERS</span></div>
                    </div>
                    <form method="post">
                        <div id="filterContent" class="filtercontent">
                            <?php foreach($filters->toArray() as $header => $filterValues) { ?>
                                <div>
                                <div><p><?php echo ($header == 'nee') ? 'Non-Executive Experience' : (($header == 'ipo') ? 'IPO Experience': ucfirst($header)); ?></p></div>
                                    <div>
                                        <span class="input-custom-holder">
                                            <input type="checkbox" value="" class="<?php echo $header; ?>_parent">
                                            <span class="fake-input"></span>
                                        </span>
                                        &nbsp;<span class="blue"> (All)</span><br>
        
                                        <?php foreach($filterValues as $parent_id => $filter) { ?>
        
                                            <?php if(!isset($filter[0])) { ?>
                                                    <span class="input-custom-holder"><input type="checkbox" value="<?php echo $filter['value']; ?>" name="<?php echo $filter['name']; ?>"
                                                           <?php echo (isset($search['filters'][$header]) && (in_array($filter['value'], $search['filters'][$header])))?'checked="checked"' : '' ?>
                                                           class="<?php echo $header; ?>">
                                                           <span class="fake-input"></span>
                                                        </span>
                                                    &nbsp;<span class="labels"> <?php echo $filter['text']; ?><br></span>
                                            <?php } else { ?>
                                                <?php foreach($filter as $subFilter) { ?>
                                                    <span class="sub_parent js-open-close">
                                                    <span class="input-custom-holder"><input type="checkbox" value="<?php echo $subFilter['value']; ?>" name="<?php echo $subFilter['name']; ?>"
                                                        <?php echo (isset($search['filters'][$header]) && (in_array($subFilter['value'], $search['filters'][$header])))?'checked="checked"' : '' ?>
                                                        class="<?php echo $header; ?>">
                                                        <span class="fake-input"></span>
                                                    </span>
                                                    <span class="labels"><?php echo $subFilter['text']; ?></span></br>
                                                    <span class="js-slider"><?php if(isset($filterValues[$subFilter['id']])) { ?>
                                                                
                                                            <?php foreach($filterValues[$subFilter['id']] as $subFilterRow) { ?>
                                                                <span class="input-custom-holder"><input type="checkbox" style="width: 45px;" value="<?php echo $subFilterRow['value']; ?>" name="<?php echo $subFilterRow['name']; ?>"
                                                                    <?php echo (isset($search['filters'][$header]) && (in_array($subFilterRow['value'], $search['filters'][$header])))?'checked="checked"' : '' ?>
                                                                    class="sub <?php echo $header; ?>">
                                                                    <span class="fake-input"></span></span>
                                                                <span class="labels"><?php echo $subFilterRow['text']; ?></span></br>
                                                            <?php } ?>
                                                                
                                                        <?php } ?></span>
                                                            </span>
                                                <?php } ?>
                                            <?php } ?>
                                            <?php if(isset($filter[0]))break; ?>
                                        <?php } ?>
        
                                    </div>
                                </div>
                            <?php } ?>
                            <!--div class="filtercontentbg"></div-->
        
                        </div>
                        <td>
                            {{--<input type="image" onclick="console.log(1);return false;" src="../images/button-search.gif" class="searchButton" id="searchButton"/>--}}
                            {{--<input type="image" onclick="search.init();" src="../images/button-search.gif" class="searchButton" id="searchButton"/>--}}
                            <!-- <input type="submit" value="" onclick="search.init();" style="width: 160px;height: 54px;background: url('../images/button-search.gif')"  class="searchButton" id="searchButton"/> -->
                            <button type="submit" onclick="search.init();" style="" class="searchButton" id="searchButton"/>Search here <i class="icon-search"></i></button>
                        </td>
                    </form>
                </div>
        
                <div class="resultsNav">
                    <table border="0" cellpadding="0" cellspacing="0" id="resultsNav">
                        <tr>
                            <td width="200px" class="left">Search Results</td>
                            <td width="80%" align="left"></td>
                            <td><a href="<?php echo $profiles->previousPageUrl(); ?>" ><img src="../images/nav-left.gif" id="leftNav" width="36" height="36" border="0" alt="" /></a></td>
                            <td>Previous</td>
                            <td><span id="pages">Page <?php echo $profiles->currentPage(); ?> of <?php echo ($profiles->lastPage() == 0) ? 1 : $profiles->lastPage(); ?></span></td>
                            <td>Next</td>
                            <td><a href="<?php echo $profiles->nextPageUrl(); ?>" ><img src="../images/nav-right.gif" id="rightNav" width="36" height="36" border="0" alt="" /></a></td>
                        </tr>
                    </table>
                    <table border="0" cellpadding="0" cellspacing="0" id="resultsNav">
                        <tr>
                            <td width="200px" class="left">Candidate Name</td>
			    <td width="200px" class="left">Plc Name</td>
                            <td width="200px" class="left">Gender</td>
                            <td width="200px" class="left">Location</td>
                            <td width="200px" class="left">Sector</td>
                            <td width="200px" class="left">IPO Experience</td>
                            <td width="200px" class="left">Portfolio Size</td>
                            <td width="200px" class="left">Capacity</td>
			    <td width="200px" class="left">EPS</td>
                        </tr>
                        <?php if($profiles->count() > 0) { ?>
                            <?php foreach($profiles as $profile) { ?>
                                <tr onclick="location.href = '/profile/<?php echo $profile->Candidate_ID; ?>';">
                                    <td width="200px" class="left"><?php echo $profile->Candidate_Known_As; ?></td>
				    <td width="200px" class="left"><?php echo $profile->Plc_Name;?>
                                    <td width="200px" class="left"><?php echo ($profile->Candidate_Gender=='M'? 'Male': 'Female'); ?></td>
                                    <td width="200px" class="left"><?php echo $profile->Candidate_City; ?></td>
                                    <td width="200px" class="left"><?php echo $profile->Adv_Sector; ?></td>
                                    <td width="200px" class="left"><?php echo ($profile->Candidate_Number_IPOs=='Y'? 'Yes':'No'); ?></td>
                                    <td width="200px" class="left"><?php echo $profile->Candidate_Portfolio_Size; ?></td>
                                    <td width="200px" class="left"><?php echo $profile->Candidate_Capacity; ?></td>
				    <td width="200px" class="left"><?php echo $profile->Plc_EPS;?></td>
                                </tr>
                            <?php } ?>
                        <?php } else { ?>
                        <tr>
                            <td colspan="4" width="100%" class="left"> Results are not found. Please change search parameters and try again. </td>
                        </tr>
                        <?php } ?>
                    </table>
                </div>
        
                <div style="overflow:auto">
                    <table border="0" cellpadding="0" cellspacing="0" id="resultsTable">
        
                    </table>
                </div>
            </div>
        </div>
        <div id="Footer"><span>&copy; DRAX 2016</span></div>
    </div>
    <script type="text/javascript" >
        $(document).ready(function(){
            $(search.criterias).each(function( i, criteria) {
                if ($(':checkbox.'+ criteria +':checked').length == $(':checkbox.' + criteria).length) {
                    $(':checkbox.'+ criteria +'_parent').prop("checked", true);
                }
            });
        });
    </script>
    </body>
@endsection
