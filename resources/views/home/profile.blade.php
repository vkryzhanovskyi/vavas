@extends('layouts.app')

@section('title', 'Profile')

@section('css')
    {!! Html::style('css/candidate.css') !!}
    {!! Html::style('css/style.css') !!}
    {!! Html::style('css/Header.css') !!}
    {!! Html::style('css/Footer.css') !!}
@endsection

@section('js')
    {!! Html::script('js/thirdParty/jquery-3.1.0.min.js') !!}
    {!! Html::script('js/thirdParty/countUp.js') !!}
    {!! Html::script('js/thirdParty/chart.bundle.min.js') !!}
    {!! Html::script('js/nav.js') !!}
    {!! Html::script('js/code.js') !!}
    {!! Html::script('js/FC.js') !!}
    {!! Html::script('js/LLDB.js') !!}
    {!! Html::script('js/Fields.js') !!}
    {!! Html::script('js/candidate.js') !!}
    {!! Html::script('js/jquery.main.js') !!}

@endsection

@section('content')

    <body>

    <div id="wrapper">
        <div class="page_common_header" id="Header">
            <div style="height:66px"></div>
            <div class="primarytabs"><!--
                --><div onclick="location.href = '/dashboard';">Dashboard</div><!--
                --><div onclick="location.href = '/search';">Search</div><!--
                --><div onclick="location.href = '/logout';">Log Out</div><!--
            --></div></div>
        
        <div id="PageOuter">
            <div id="PageInner">
                <div id="outerProfile">
                    <div class="candidateName">
                        <div  style="display:inline-block">
                            <span id="CandidateName" style="margin: 20px 0px 0px 20px;"></span>
                        </div>
                    </div>
                    <div class="innerProfile" style="width=100%;">
                        <h2><?php echo $profile->Candidate_Known_As . " (" . $profile->companies->Plc_Name . ")"; ?></h2>
                        <div id="profileTabs" style="padding-left: 12px;">
                            <div id="profileCorporateTab" onclick="candidate.showTab('profileCorporate')" class="profileTab">Corporate</div>
                            <div id="profilePersonalTab" onclick="candidate.showTab('profilePersonal')" class="profileTab">Personal</div>
                            <div id="profileRefereesTab" onclick="candidate.showTab('profileReferees')" class="profileTab">Referees</div>
                            <div id="profilePlcTab" onclick="candidate.showTab('profilePlc')" class="profileTab">Plc Performance</div>
                        </div>
                        <div class="candidateProfile" class="candidateLarge">
                            <div id="profileCorporate">
                                <div class="corporateInnerContent">
                                    <div class="corporateInnerPanel">
                                        <h2 style="margin-left:0;margin-top:8px;margin-bottom:8px;font-size:18px;">Corporate Profile</h2>
                                        <table id="candidateCorporateTable"  class="innerContentTale">
                                            <tr>
                                                <td class="profileHomeLabel">B2B/B2C/B2G:</td>
                                                <td id="B2X"><?php echo $profile->Candidate_B2X; ?></td>
        
                                            </tr>
                                            <tr><td class="profileHomeLabel">Sectors:</td>
                                                <td id="Sector">
                                                    <?php $sectors = []; ?>
                                                    <?php foreach($profile->sectors as $sector) { ?>
                                                        <?php $sectors[] = $sector['Adv_Sector']; ?>
                                                        <?php } ?>
                                                    <?php echo rtrim(implode(', ', $sectors), ', '); ?>
                                                </td>
                                            </tr>
                                            <tr><td class="profileHomeLabel">Portfolio Size:</td>
                                                <td id="PortfolioSize"><?php echo $profile->Candidate_Portfolio_Size; ?></td>
                                            </tr>
                                            <tr><td class="profileHomeLabel">IPO Experience?:</td>
                                                <td id="IPOs"><?php echo ($profile->Candidate_Number_IPOs=='Y'? 'Yes' : 'No') ; ?></td>
                                            </tr>
                                            <tr><td class="profileHomeLabel">Basic Earnings Per Share:</td>
                                                <td id="EPS"><?php echo $profile->companies->Plc_EPS; ?></td>
                                            </tr>
                                            <tr><td class="profileHomeLabel">Plc Revenues 5-year Compound Annual Growth Rate:</td>
                                                <td id="CAGR"></td>
                                            </tr>
                                            <tr><td class="profileHomeLabel">Age:</td>
                                                <td id="Age"><?php echo $profile->age; ?></td>
                                            </tr>
                                            <!--<tr><td class="profileHomeLabel">Most Recent Role:</td><td id="RecentRole"></td></tr>
                                            <tr><td class="profileHomeLabel">Available:</td><td id="Availability"></td></tr>
                                            <tr><td class="profileHomeLabel">From:</td><td id="AvailableFrom"></td></tr>
                                            <tr><td class="profileHomeLabel">Seeking:</td><td id="Seeking"></td></tr>-->
                                        </table>
                                        <div id="careerHist">
                                            <h2 style="margin-bottom:8px;font-size:18px;">Career History</h2>
                                            <table  class="innerContentTable">
						<tbody id="careerHistoryTable"><tr><td><?php echo $profile->Candidate_Bio; ?></td></tr></tbody>
                                                <tbody id="careerHistoryTable">
                                                    <?php foreach($profile->careers as $career) { ?>
                                                        <tr>
                                                            <td class="profileHomeLabel"><?php echo $career->Career_Employer ?></td>
                                                            <td><?php echo $career->Career_Employer_Ownership; ?></td>
                                                            <td><?php echo $career->Career_Role; ?></td>
                                                            <td><?php echo $career->Career_Start; ?></td>
                                                            <td><?php echo $career->Career_End; ?></td>
                                                        </tr>
                                                    <?php } ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="profilePersonal" style="display: none">
                                <div class="corporateInnerPanel">
                                    <h2 style="margin-left:0;margin-top:8px;margin-bottom:8px;font-size:18px;">Personal Details</h2>
                                    <div id="candidateBasic" class="profileInnerContent" style="padding-left:0;">
                                        <table>
                                            <tr><td class="profileHomeLabel">Location:</td>
                                                <td id="Location"><?php echo $profile->Candidate_City; ?></td>
                                            </tr>
                                            <tr><td class="profileHomeLabel">Marital Status:</td>
                                                <td id="Marital">LOCKED</td>
                                            </tr>
                                            <tr><td class="profileHomeLabel">Dependents:</td>
                                                <td id="Dependents">LOCKED</td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div id="candidateContact" class="profileInnerContent">
                                        <table>
                                            <tr><td class="profileHomeLabel">Phone Number:</td>
                                                <td id="Phone">LOCKED</td>
                                            </tr>
                                            <tr><td class="profileHomeLabel">Email:</td>
                                                <td id="Email">LOCKED</td>
                                            </tr>
                                            <tr><td class="profileHomeLabel">Links:</td>
                                                <td id="Connect">LOCKED</td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div id="candidateEducation" class="wideProfileInnerContent" style="padding-left:0">
                                        <h2 style="margin-bottom:8px;font-size:18px;">Education</h2>
                                        <table>
                                            <tbody id="educationTable">
                                                <?php foreach($profile->eductaiton as $eductaiton) { ?>
                                                    <tr>
                                                        <td><?php echo $eductaiton->Education_Institution ?></td>
                                                        <td><?php echo $eductaiton->Education_Subject; ?></td>
                                                        <td><?php echo $eductaiton->Education_Start; ?></td>
                                                        <td><?php echo $eductaiton->Education_End; ?></td>
                                                    </tr>
                                                <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>
				    <div>
					<h2 style="margin-bottom:8px;font-size:18px">Career History</h2>
					<table class="innerContentTable">
						<tbody>
							<?php foreach($profile->careers as $career) { ?>
								<tr>
									<td><?php echo $career->Career_Employer ; ?></td>
							                <td><?php echo $career->Career_Employer_Ownership ; ?></td>
							                <td><?php echo $career->Career_Role ; ?></td>
							                <td><?php echo $career->Career_Start ; ?></td>
							                <td><?php echo $career->Career_End ; ?></td>
								</tr>
							<?php } ?>
						</tbody>
					</table>
				    </div>
                                </div>
                            </div>
                            <div id="profileReferees" style="display: none" class="network">
                                <h2 style="margin-bottom:8px;padding-top:8px;padding-left:20px;font-size:18px;">Drax Referees</h2>
                                <div style="margin: 20px 20px 10px 20px">
                                    <table id="draxReferences">
                                        <?php foreach($profile->draxreferences as $draxreferences) { ?>
                                        <tr>
                                            <td><?php echo $draxreferences->Reference_Name ?></td>
                                            <td><?php echo $draxreferences->Reference_Company; ?></td>
                                        </tr>
                                        <?php } ?>
                                    </table>
                                </div>
                                <h2 style="margin-bottom:8px;padding-left:20px;font-size:18px;">Co-Directors</h2>
                                <div id="" style="margin: 20px 20px 10px 20px">
                                    <table id="coDirectors">
                                        <?php foreach($profile->codirectors as $codirectors) { ?>
                                        <tr>
                                            <td><?php echo $codirectors->Reference_Name ?></td>
                                            <td><?php echo $codirectors->Reference_Company; ?></td>
                                        </tr>
                                        <?php } ?>
                                    </table>
                                </div>
                            </div>
                            <div id="profilePlc" style="display: none">
                                <div class="corporateInnerPanel">
                                    <h2 style="margin-left:0;margin-top:8px;margin-bottom:8px;font-size:18px;text-align:center">Plc Performance</h2>
                                    <div id="revChartDiv" style="height: auto"><canvas id="revChart"></canvas></div>
                                    <div id="EBITDAChartDiv"><canvas id="ebitdaChart"></canvas></div>
                                    <div width="100%">
                                        <table id="plcFinancials" class="wideProfileInnerContent" display="float:left">
                                            <thead>
                                            <td><b>Financial Performance (&pound;m)</b></td>
                                            <td><b>2016</b></td>
                                            <td><b>2015</b></td>
                                            <td><b>2014</b></td>
                                            <td><b>2013</b></td>
                                            <td><b>2012</b></td>
					    <td><b>2011</b></td>
                                            </thead>
                                            <tr id="RevTblRow">
                                                <td><b>Revenues</b></td>
                                                <td><?php echo  number_format((double)$profile->companies->Plc_Rev_5, 2, '.', ''); ?></td>
                                                <td><?php echo  number_format((double)$profile->companies->Plc_Rev_4, 2, '.', ''); ?></td>
                                                <td><?php echo  number_format((double)$profile->companies->Plc_Rev_3, 2, '.', ''); ?></td>
                                                <td><?php echo  number_format((double)$profile->companies->Plc_Rev_2, 2, '.', ''); ?></td>
                                                <td><?php echo  number_format((double)$profile->companies->Plc_Rev_1, 2, '.', ''); ?></td>
						<td><?php echo  number_format((double)$profile->companies->Plc_Rev_0, 2, '.', ''); ?></td>
        
                                            </tr>
                                            <tr id="EBITDATblRow">
                                                <td><b>EBITDA</b></td>
                                                <td><?php echo  number_format((double)$profile->companies->Plc_EBITDA_5, 2, '.', ''); ?></td>
                                                <td><?php echo  number_format((double)$profile->companies->Plc_EBITDA_4, 2, '.', ''); ?></td>
                                                <td><?php echo  number_format((double)$profile->companies->Plc_EBITDA_3, 2, '.', ''); ?></td>
                                                <td><?php echo  number_format((double)$profile->companies->Plc_EBITDA_2, 2, '.', ''); ?></td>
                                                <td><?php echo  number_format((double)$profile->companies->Plc_EBITDA_1, 2, '.', ''); ?></td>
						<td><?php echo  number_format((double)$profile->companies->Plc_EBITDA_0, 2, '.', ''); ?></td>
        
                                            </tr>
                                        </table>
                                        <div id="financialsRight">
                                            <table>
                                                <tr><td><b>EPS:</b></td><td id="PlcEPS"</td><?php echo $profile->companies->Plc_EPS; ?></tr>
                                                <tr><td><b>CAGR:</b></td><td id="PlcCAGR"</td></tr>
                                            </table>
                                        </div>
                                    </div>
                                    <h2 style="margin-left:0;margin-top:15px;margin-bottom:8px;font-size:18px;text-align:center">Plc General Details</h2>
                                    <div id="PlcBasicLeft" style="float:left" width="50%"">
                                    <table>
                                        <tr><td><b>Location:</b></td>
                                            <td id="PlcLocation"><?php echo $profile->companies->Plc_Location; ?></td>
                                        </tr>
                                        <tr><td><b>Sector:</b></td>
                                            <td id="PlcSector"><?php echo $profile->companies->Plc_Adv_Sector; ?></td>
                                        </tr>
                                    </table>
                                </div>
                                <div id="candidateContact" style="float:right" width="50%">
                                    <table>
                                        <tr><td><b>Website:</b></td>
                                            <td id="PlcWebsite"><a href=<?php echo "http:\/\/" . $profile->companies->Plc_URL;?> target="_blank"><?php echo $profile->companies->Plc_URL; ?></a></td>
                                        </tr>
                                        <tr><td><b>Business Description:</b></td>
                                            <td id="PlcBD"><?php echo $profile->companies->Plc_BD; ?></td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        
            <a href="{{ URL::previous() }}" class="buttonReturn"><span><</span> Return to search</a>
            <!-- <a href="{{ URL::previous() }}" ><img id="returnImage" src="../images/button-return-search.gif" width="193" height="53" border="0" alt="Return to Search" class="buttonReturn" />Return to search</a> -->
        </div>
        </div>
        <div id="Footer"><span>&copy; DRAX 2016</span></div>
    </div>
    <script type="text/javascript">
        revs = [
            <?php echo $profile->companies->Plc_Rev_5; ?>,
            <?php echo $profile->companies->Plc_Rev_4; ?>,
            <?php echo $profile->companies->Plc_Rev_3; ?>,
            <?php echo $profile->companies->Plc_Rev_2; ?>,
            <?php echo $profile->companies->Plc_Rev_1; ?>,
	    <?php echo $profile->companies->Plc_Rev_0; ?>

        ];
        ebitdas = [
            <?php echo $profile->companies->Plc_EBITDA_5; ?>,
            <?php echo $profile->companies->Plc_EBITDA_4; ?>,
            <?php echo $profile->companies->Plc_EBITDA_3; ?>,
            <?php echo $profile->companies->Plc_EBITDA_2; ?>,
            <?php echo $profile->companies->Plc_EBITDA_1; ?>,
	    <?php echo $profile->companies->Plc_EBITDA_0; ?>

        ];
        cargRevs = [
            <?php echo $profile->companies->Plc_Rev_5; ?>,
            <?php echo $profile->companies->Plc_Rev_4; ?>,
            <?php echo $profile->companies->Plc_Rev_3; ?>,
            <?php echo $profile->companies->Plc_Rev_2; ?>,
            <?php echo $profile->companies->Plc_Rev_1; ?>,
            <?php echo $profile->companies->Plc_Rev_0; ?>

        ];
        $cagr = cagr(cargRevs);
        candidate.buildRevs(revs);
        candidate.buildEBITDAs(ebitdas)
        $('#CAGR').html($cagr);
        $('#PlcCAGR').html($cagr);
    </script>
    </body>
@endsection
