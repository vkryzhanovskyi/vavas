
@extends('layouts.app')

@section('title', 'Home Page')

@section('css')
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
{!! Html::style('css/main.css') !!}
@endsection

@section('js')
    {!! Html::script('js/thirdParty/jquery-3.1.0.min.js') !!}
    {!! Html::script('js/jquery.main.js') !!}

@endsection

@section('content')
<body>
    <div class="container">
        <div class="login-background" >
            <div class="jumbotron center-block vertical-center " >
                <?php if (!Auth::check()) { ?>
                <p>
                    {{ $errors->first('user') }}
                    {{ $errors->first('password') }}
                </p>
                {{ Form::open(array('url' => 'login', 'class' => 'form-signin')) }}
                    <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
                    <label for="inputUsername" class="sr-only">Username</label>
                    <input type="text" id="inputUsername" class="form-control" placeholder="Username" required="" autofocus="" name="user">
                    <label for="inputPassword" class="sr-only">Password</label>
                    <input type="password" id="inputPassword" class="form-control" placeholder="Password" required="" name="password">
                    <button class="btn btn-lg btn-primary btn-block" type="submit">Login</button>
                    <span class="text-muted">By logging in, you agree to be bound by the terms of DRXDATA set out <div class="popup-holder">
                     <a class="open" href="#">here</a>.
                     <div class="popup">
                        <div class="popup-header text-center">
                            <h2>TERMS AND CONDITIONS</h2>
                        </div>
                        <div class="popup-body">
                            <div class="popup-subheader text-center">
                                <p><strong>Drax Executive Ltd &ndash;Database Online Terms and Conditions</strong></p>
                                <p><strong>TERMS AND CONDITIONS OF USE</strong></p>
                            </div>
                            <p>The Drax Executive Database (&ldquo;<strong>Database</strong>&rdquo;) is owned and operated by Drax Executive Ltd ("<strong>We</strong>/<strong>Us/Our</strong>").</p>
                            <p>By using Our Database, you confirm that you accept these terms and conditions of use (&ldquo;<strong>Terms</strong>&rdquo;) and that you agree to comply with them. If you do not agree to these Terms, you must not use Our Database. We recommend that you print a copy of these Terms for future reference.</p>
                            <p>You will be responsible for making your own arrangements to access the Database, including obtaining licences for any software required to access, query or analyse the Database and any Data obtained from it.</p>
                            <p><strong>We may make changes to these terms</strong></p>
                            <p>We may amend these Terms from time to time. Every time you wish to use Our Database, please check these Terms to ensure you understand the Terms that apply at that time.</p>
                            <p><strong>We may make changes to Our Database</strong></p>
                            <p>We shall have editorial control over the Database and its content and may change its nature, format and other features at any time without notice.</p>
                            <p><strong>We may suspend or withdraw Our Database</strong></p>
                            <p>We do not guarantee that Our Database, or any content on it, will always be available or be uninterrupted. We may suspend or withdraw or restrict the availability of all or any part of Our Database for business and operational reasons. We will try to give you reasonable notice of any suspension or withdrawal.</p>
                            <p>You are responsible for ensuring that all persons who access Our Database through your internet connection are aware of these Terms, and that they comply with them.</p>
                            <p>Where access to the Database is made available to you with a user name and password to enable access, you shall keep the user name and password secret and confidential and shall not, without Our prior written consent, share them with any other person. You shall notify Us immediately on becoming aware that any username or password has become known to any other person. We may at any time require you to change any username or password.</p>
                            <p><strong>How you may use material on Our Database</strong></p>
                            <p>We are the owner or the licensee of all intellectual property rights in Our Database. Those works are protected by copyright laws, database laws and treaties around the world. All such rights are reserved.</p>
                            <p>Subject to the payment of such licence fees as We may charge from time to time to allow access to Our Database or such access as you may be granted to access Our Database in connection with an assignment with Us (&ldquo;<strong>Retained Assignment</strong>&rdquo;), We hereby grant to you by way of non-exclusive, non-transferable licence the right during the Duration to use the Data obtained from the Database for your own Internal Purposes.</p>
                            <p>Save as expressly permitted in these Terms you shall not without Our prior written consent:</p>
                            <ul>
                            <li>use the Database for any purpose other than as expressly permitted under these Terms;</li>
                            <li>merge Data with any other data or information, alter or manipulate the Data or incorporate the Data or any part of it into any other publication, document or materials in any media;</li>
                            <li>copy the Data (other than in the course of normal use pursuant to by these Terms and backing up) or disclose or distribute it to others;</li>
                            <li>share or distribute the Data with or to any third party;</li>
                            <li>disassemble or attempt to disassemble the encryption systems or other security features which may be incorporated in the Database;</li>
                            <li>remove, alter or amend any notices within the Data concerning its origin or ownership.</li>
                            </ul>
                            <p><strong>Our intellectual property rights </strong></p>
                            <p>All intellectual property rights (&ldquo;<strong>Intellectual Property Rights</strong>&rdquo;) in and to the Database and the Data are and shall at all times remain Our exclusive property.</p>
                            <p>Where We consent to the merger of any Data with any other data or information, the alteration or manipulation of the Data or the incorporation of the Data or any part of it into any other publication, document or materials in any media which would otherwise be prohibited by these Terms3.2(b), then all Intellectual Property Rights and all other rights in and to the Data shall remain owned by Us notwithstanding such consent.</p>
                            <p>Save for the rights hereby expressly licensed to you7.4, nothing in these Terms shall be construed as granting, assigning, creating or transferring to you any right, title or interest in or to the Intellectual Property Rights in and to the Database and/or the Data.</p>
                            <p>If We grant Our consent as mentioned above then3.2(c) whenever and wherever you reproduce or extract any of the Data or any part thereof for any purpose you shall, in such manner as We may direct from time to time, quote Us as the source and indicate Us as the owner of the Intellectual Property Rights in such Data.</p>
                            <p><strong>Duration, termination and suspension</strong></p>
                            <p>These Terms shall commence at the time you first confirm your agreement to them by indicating your acceptance below, or your use of the Database (whichever occurs first) and shall continue until the termination or expiry of these Terms unless terminated earlier in accordance with the remaining provisions of this clause (<strong>&ldquo;Duration&rdquo;</strong>)8.</p>
                            <p>Without prejudice to any other rights or remedies that may be available to Us, We may by notice in writing to you suspend the licence hereby granted if you are in breach of any of the terms of these Terms.</p>
                            <p>We shall be entitled to terminate these Terms immediately by written notice to the you if:</p>
                            <ul>
                            <li>you are in material or persistent breach of any of your obligations under these Terms and have not remedied such breach (if capable of remedy) within fourteen days of written notice from Us detailing the breach and requiring you to remedy it; or</li>
                            <li>you have a receiver (including an administrative receiver), administrator or manager appointed over the whole or any part of your assets, or if any order is made or a resolution passed for your winding up (except for the purpose of amalgamation or reconstruction), or if you shall enter into any composition or arrangement with your creditors or if you cease or threaten to cease to carry on business.</li>
                            </ul>
                            <p><strong>Consequences of termination</strong></p>
                            <p>On termination of these Terms, howsoever caused, all rights that you have in or in relation to the Database and/or the Data shall immediately end.</p>
                            <p><strong>Our liability</strong></p>
                            <p>While we have made all reasonable efforts to ensure that the Data in the Database is accurate We do not warrant the accuracy of the Data and We shall not be liable to you in contract, tort or otherwise for loss of profits, goodwill, business or anticipated savings or for any indirect or consequential loss as a result of any errors in the Data or the Database.</p>
                            <p>Our total aggregate liability to you arising out of or in connection with these Terms in contract, tort or otherwise shall in no event exceed the licence fees (if any) paid by you to Us for access to the Database during the previous twelve months.</p>
                            <p>Nothing in these Terms shall be construed as limiting the liability Our liability for death or personal injury caused by Our negligence or for fraud.</p>
                            <p><strong>Data protection </strong></p>
                            <p>The parties recognise that certain information that We collect and certain Data shall constitute personal data in respect of which We shall be the data controller for the purposes of all applicable data protection laws.</p>
                            <p>You shall use any such personal data in accordance with all applicable data protection laws and 11.1solely to the extent permitted under these Terms or otherwise as permitted by applicable law from time to time.</p>
                            <p><strong>Confidentiality</strong></p>
                            <p>&ldquo;<strong>Confidential Information</strong>&rdquo; means the Data, any usernames and passwords allocated under these Terms and all other information acquired by you as a result of your use of the Database.</p>
                            <p>Other than as expressly permitted by these Terms, you shall keep confidential and not disclose or procure or permit the disclosure of the Confidential Information.</p>
                            <p>Your obligations under this clause 12do not apply to any Confidential Information which:</p>
                            <ul>
                            <li>you can demonstrate that you were in its possession before you acquired it from Us or that you had acquired from a third party entitled to disclose it;</li>
                            <li>has become public knowledge other than through disclosure by you in breach of these Terms;</li>
                            <li>is authorised by Us in advance in writing to be released;</li>
                            <li>is required to be disclosed by law or pursuant to an order of court or other competent government or regulatory authority, provided that prior to any such disclosure you will notify Us in writing of such disclosure and you shall use all reasonable endeavours to prevent such disclosure and to ensure that any person to whom the Confidential Information is disclosed is aware of the confidential nature of the information and takes steps to prevent further disclosure of the same.</li>
                            </ul>
                            <p>The provisions of this clause will12 survive termination of these Terms for whatever reason.</p>
                            <p><strong>Notices</strong></p>
                            <p>Any notice or other communication given or required to be given under these Terms shall be in writing and may be sent either by email or first class post to the address of the party on which it is served as notified to the other party from time to time.</p>
                            <p>Any notice sent shall be deemed to have been received (a) in the case of email immediately on receipt and (b) in the case of first class post, on the date falling two days (excluding Saturdays, Sundays and public holidays) after the date on which such notice is sent.</p>
                            <p><strong>General Conditions</strong></p>
                            <p>In these Terms:</p>
                            <p><strong>&ldquo;Data&rdquo; </strong>means all data obtained by you from the Database as a result of your use of the Database.</p>
                            <p><strong>&ldquo;Internal Purposes&rdquo; </strong>means purposes which are internal to your business, but not, in any event, the purposes of any other business carried on by you or by any other person or for the purposes of resale or the provisions of services to any third party using such Data.</p>
                            <p>These Terms and Conditions constitutes the entire agreement, and supersedes any previous agreement, arrangement, representations or understanding (whether oral or written), between the parties relating to its subject matter.</p>
                            <p>If any provision of these Terms is to any extent invalid or unenforceable that will not affect the validity or enforceability of the remainder of the provision or of any other provision.</p>
                            <p>You may not assign the benefit or your obligations under these Terms without Our written consent.</p>
                            <p>Neither of us shall be liable to the other for any failure or delay in the performance of its obligations under these Terms caused by any act of God, bad weather, fire, power cut, the act or omission of any government or other public authority, war, riot, or any circumstances beyond our reasonable control.</p>
                            <p>A failure to exercise or delay in exercising any right or remedy under these Terms or by law does not constitute a waiver of that or any other right or remedy.</p>
                            <p>The headings contained in these Terms are for reference purposes only and are not intended to affect its interpretation.</p>
                            <p>These Terms are governed by English law and the parties agree to submit to the exclusive jurisdiction of the English Courts.</p>
                        </div>
                        <div class="popup-footer">
                            <a class="close" href="#">Close</a>
                        </div>
                     </div>
                    </div>
                    </span>

                {{ Form::close() }}
                <?php } else { ?>
                    <span class="text-muted">Hi, <?php echo Auth::user()->user; ?></span>
                    <a href="/dashboard">Enter</a>
                <?php } ?>
            </div>
        </div>
    </div>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>
</body>

@endsection
