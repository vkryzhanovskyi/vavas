candidate = function(){}
candidate.onLoad = function(){
    //When we navigate to a candidate profile, load the 'CID' localStorage variable - this gets us to the indivudal candidate's profile.
    var CID = localStorage.CID;
    candidate.populateCandidateProfile(CID)
}

candidate.populateCandidateProfile = function(CID){
    LiveListDB.execSQL('SELECT * FROM Candidates inner join companies on Candidates.Candidate_Current_Plc_ID=Companies.Plc_ID where Candidate_ID=?',[CID],function(t,r){
        //get the candidate, and the profile of their current plc 
        candidate.populateFromCandidates(r.rows.item(0))
        candidate.populateCompany(r.rows.item(0));
    })
    LiveListDB.execSQL('SELECT * FROM Careers WHERE Candidate_ID=?',[CID],function(t,r){
        //Get the careers rows for the candidate
        if(r.rows.length>0){candidate.populateCareers(r.rows)}
    })
    LiveListDB.execSQL('SELECT * FROM Education WHERE Candidate_ID=?',[CID],function(t,r){
        //Get the Education rows for the candidate 
        if(r.rows.length>0){candidate.populateEducation(r.rows)}
    })
    LiveListDB.execSQL('SELECT * FROM coDirectors WHERE Candidate_ID=?',[CID],function(t,r){
        //Get the coDirectors for the candidate
        if(r.rows.length>0){candidate.populateCoDirectors(r.rows)}
    })
    LiveListDB.execSQL('SELECT * FROM draxReferences WHERE Candidate_ID=?',[CID],function(t,r){
        //Get the draxReferences fields for the candidate (a separate source for the coDirectors) 
        if(r.rows.length>0){candidate.populateDraxReferences(r.rows)}
    })
    LiveListDB.execSQL('SELECT * FROM sectors WHERE Candidate_ID=?',[CID],function(t,r){
        //Get the candidate sectors
        if(r.rows.length>0){candidate.populateSectors(r.rows)}
    })
}

candidate.populateFromCandidates = function(c){
    console.log(c)
    //Where the data can be populated from Candidates, grab it from there
    FCSetInnerText(document.getElementById('CandidateName'),toTitleCase(c.Candidate_Full_Name))
    FCSetInnerText(document.getElementById('B2X'),c.Candidate_B2X)
    FCSetInnerText(document.getElementById('Location'),c.Candidate_City);
    FCSetInnerText(document.getElementById('PortfolioSize'),c.Candidate_Portfolio_Size);
    FCSetInnerText(document.getElementById('IPOs'),c.Candidate_Number_IPOs);
    FCSetInnerText(document.getElementById('Marital'),c.Candidate_Marital_Status);
    FCSetInnerText(document.getElementById('Dependents'),c.Candidate_Dependents)
    FCSetInnerText(document.getElementById('Phone'),c.Candidate_Phone_Number)
    FCSetInnerText(document.getElementById('Email'),c.Candidate_Email)
    FCSetInnerText(document.getElementById('EPS'),c.Plc_EPS)
    FCSetInnerText(document.getElementById('PlcEPS'),c.Plc_EPS)
    FCSetInnerText(document.getElementById('CAGR'),c.Plc_CAGR)
    FCSetInnerText(document.getElementById('PlcCAGR'),c.Plc_CAGR)
    FCSetInnerText(document.getElementById('Age'),candidate.calculate_age(c.Candidate_DoB))
}

candidate.populateSectors = function(c){
    var pushArr = []
    for (i in Object.keys(c)){pushArr.push(c[i].Adv_Sector)}
    //Set the innerText to the comma separation of the sectors
    FCSetInnerText(document.getElementById('Sector'),toTitleCase(pushArr.join(', ')))
}

candidate.showTab = function(tabToShow){
    //simple tab navigation script - just hide things when we don't want them.
    var tabs = ['profileCorporate','profilePersonal','profileReferees','profilePlc'];
    tabs.forEach(function(tab){
        var tabDOM = document.getElementById(tab);
        if(tab==tabToShow){tabDOM.style.display = "block"} else {tabDOM.style.display = "none"}
    });
}

candidate.addURL = function(connectElement, url, txt)
{
	if (url != null && url != '')
	{
		// note that these are normal a links, we want them to open in an external safari
		var a = document.createElement('a');
		a.href = url;
		a.target = "_blank";
        a.innerText = txt;
		connectElement.appendChild(a);
	}
}

candidate.populateCareers = function(careers){
    console.log(careers)
    if(careers.length>0){
        var cTable = document.getElementById('careerHist')
        //for each element we get back for a candidate career, add a row of employer|employer ownership|role|start|end.
        for(var i = 0;i<careers.length;i++){
            var cLine = careers[i]
            var row = document.createElement('tr') 
            row.innerHTML = '<td>'+cLine.Career_Employer.replace(/[^a-zA-Z0-9 ]/g, "")+'</td><td>'+cLine.Career_Employer_Ownership.replace(/[^a-zA-Z0-9 ]/g, "")+'</td><td>'+cLine.Career_Role.replace(/[^a-zA-Z0-9 ]/g, "")+'</td><td>'+cLine.Career_Start.replace(/[^a-zA-Z0-9 ]/g, "")+' - '+cLine.Career_End.replace(/[^a-zA-Z0-9 ]/g, "")+'</td>'
            cTable.appendChild(row)
        }
    } else{
        document.getElementById('cTable').style = 'display:none'
    }
}

candidate.populateEducation = function(edu){
    console.log(edu)
    if(edu.length>0){
        var eduTbl = document.getElementById('educationTable')
        //similar setup to the careers point.
        for(var i = 0;i<edu.length;i++){
            var edLine = edu[i]
            var row = document.createElement('tr') 
            if(edLine.Education_Subject.replace(/[^a-zA-Z0-9 ]/g, "")!=""){row.innerHTML = '<td>'+edLine.Education_Subject.replace(/[^a-zA-Z0-9 ]/g, "")+'</td>'}
            row.innerHTML+="<td>"+edLine.Education_Institution.replace(/[^a-zA-Z0-9 ]/g, "")+'</td><td>'+edLine.Education_Start.replace(/[^a-zA-Z0-9 ]/g, "")+' - '+edLine.Education_End.replace(/[^a-zA-Z0-9 ]/g, "")+'</td>'
            eduTbl.appendChild(row)
        }
    } else{
        document.getElementById('candidateEducation').style = 'display:none'
    }
}
candidate.populateCompany = function(c){
    //populate the basic company information: revs, ebitdas, location, sector, website.
    var revs = [c.Plc_Rev_0,c.Plc_Rev_1,c.Plc_Rev_3,c.Plc_Rev_4,c.Plc_Rev_5]
    revs.forEach(function(i){candidate.addFinancialsToTbl(i,'RevTblRow')})
    var ebitdas = [c.Plc_EBITDA_0,c.Plc_EBITDA_1,c.Plc_EBITDA_3,c.Plc_EBITDA_4,c.Plc_EBITDA_5]
    ebitdas.forEach(function(i){candidate.addFinancialsToTbl(i,'EBITDATblRow')})
    candidate.buildRevs(revs)
    candidate.buildEBITDAs(ebitdas)
    FCSetInnerText(document.getElementById('PlcLocation'),c.Plc_Location)
    FCSetInnerText(document.getElementById('PlcSector'),c.Plc_Sector)
    FCSetInnerText(document.getElementById('PlcWebsite'),c.Plc_URL)
    FCSetInnerText(document.getElementById('PlcBD'),c.Plc_BD)
}
candidate.addFinancialsToTbl = function(num,rowid){
    //adds financial information to the row as a td, with comma separation.
    var row = document.getElementById(rowid);
    var item = document.createElement('td');
    item.innerText = num.replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    row.appendChild(item)
}

candidate.buildRevs = function(revs){
    //use chart.min.js to build a chart of revenues going back 2016-2012 (the most we can scrape from MIQ)
    var revchart = $('#revChart') 
    var data = {
    labels: ['2011','2012','2013','2014','2015','2016'],
    datasets: [
        {
            label: "Turnover",
            fill: false,
            lineTension: 0.1,
            backgroundColor: "rgba(75,192,192,0.4)",
            borderColor: "rgba(75,192,192,1)",
            borderCapStyle: 'butt',
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: 'miter',
            pointBorderColor: "rgba(75,192,192,1)",
            pointBackgroundColor: "#fff",
            pointBorderWidth: 1,
            pointHoverRadius: 5,
            pointHoverBackgroundColor: "rgba(75,192,192,1)",
            pointHoverBorderColor: "rgba(220,220,220,1)",
            pointHoverBorderWidth: 2,
            pointRadius: 1,
            pointHitRadius: 10,
            data: revs,
            spanGaps: false,
            }
        ]
    };
    var revChart = new Chart(revchart,{
        type:'line',
        data: data,
        options: {
            title: {
                display:true,
                text: 'Turnover'
            },
            legend: {
                display:false
            },
            scales: {
                yAxes: [
                    {
                        ticks: {
                            callback: function(label, index, labels) {
                            return '\u00A3'+label+'m';
                            }
                        }
                    }
                ]
            }
        }
    })
}
candidate.buildEBITDAs = function(ebitdas){
    //do the same for EBITDA
    var ebitdachart = $('#ebitdaChart') 
    var data = {
    labels: ['2011','2012','2013','2014','2015','2016'],
    datasets: [
        {
            label: "EBITDA",
            fill: false,
            lineTension: 0.1,
            backgroundColor: "rgba(75,192,192,0.4)",
            borderColor: "rgba(75,192,192,1)",
            borderCapStyle: 'butt',
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: 'miter',
            pointBorderColor: "rgba(75,192,192,1)",
            pointBackgroundColor: "#fff",
            pointBorderWidth: 1,
            pointHoverRadius: 5,
            pointHoverBackgroundColor: "rgba(75,192,192,1)",
            pointHoverBorderColor: "rgba(220,220,220,1)",
            pointHoverBorderWidth: 2,
            pointRadius: 1,
            pointHitRadius: 10,
            data: ebitdas,
            spanGaps: false,
            }
        ]
    };
    var ebitdaChart = new Chart(ebitdachart,{
        type:'line',
        data: data,
        options: {
            title: {
                display:true,
                text: 'EBITDA'
            },
            legend: {
                display:false
            },
            scales: {
                yAxes: [
                    {
                        ticks: {
                            callback: function(label, index, labels) {
                            return '\u00A3'+label+'m';
                            }
                        }
                    }
                ]
            }
        }
    })
}

candidate.populateDraxReferences = function(c){
    //quick one-liner: take the references from draxReferences and pass them to addCandidateToRefs
    for(i=0;i<c.length;i++){candidate.addCandidateToRefs(c[i].Reference_Name,c[i].Reference_Company,'draxReferences')}
}

candidate.populateCoDirectors = function(c){
    //quick one-liner: take the references from coDirectors and pass them to addCandidateToRefs
    for(i=0;i<c.length;i++){candidate.addCandidateToRefs(c[i].Reference_Name,c[i].Reference_Company,'coDirectors')}
}

candidate.addCandidateToRefs = function(RefName,CoName,elemID){
    //adds lines for references of any type.
    var addTo = document.getElementById(elemID);
    var tr = document.createElement('tr')
    var td1 = document.createElement('td');td1.innerText = RefName;tr.appendChild(td1);
    var td2 = document.createElement('td');td2.innerText = CoName;tr.appendChild(td2);
    addTo.appendChild(tr);
}
candidate.calculate_age = function(dob)
{
    //clever script from stack overflow - takes birth date and gives back age.
    //possibly a sledge hammer to crack a walnut, but better than getting it wrong.
    today_date = new Date();
    today_year = today_date.getFullYear();
    today_month = today_date.getMonth();
    today_day = today_date.getDate();
    
    birth_year = dob.substring(0,4)
    birth_month = dob.substring(5,7)
    birth_day = dob.substring(8,10)
    
    age = today_year - birth_year;

    if ( today_month < (birth_month - 1))
    {
        age--;
    }
    if (((birth_month - 1) == today_month) && (today_day < birth_day))
    {
        age--;
    }
    return age;
}
