mainSplash = function(){}

//populates from internal db - this will need to move to PHP injection once it's a live db.
mainSplash.onLoad = function(){
    if(localStorage.LLRedirectTo){localStorage.removeItem('LLRedirectTo')}
        //Get counts of CEOs and set innertext for CEOCount
    LiveListDB.execSQL('SELECT * FROM rns ORDER BY RNS_Date DESC LIMIT 100',[],function(tr,res){
        if(res.rows.length>0){mainSplash.populateRns(res.rows)}
    })
}
mainSplash.populateRns = function(res){
    for(i=0;i<res.length;i++){mainSplash.addRNS(res[i])}
}

mainSplash.addRNS = function(row){
    var item = document.createElement('div')
    item.className = 'RNSItem'
    item.innerHTML = '<a href="#" onclick="">'+row.RNS_Headline+'</a>'+' - '+row.RNS_Date+'<br>'+row.RNS_Body
    document.getElementById('RNS').appendChild(item)
}