﻿function SearchCommon()
{
	//legacy code from DEC - reused for convenience.
}

SearchCommon.createCheckboxes = function(tr, title, names, values)
{
	var enableDisableCheckbox = function()
	{
		// 'this' will refer to the component
		if (this.className == 'off')	this.className = 'on';
		else							this.className = 'off';
	};
	var maindiv = FCCreateAndAdd(tr,'div')
	// create the td
	//parentElement, type, id, className, value
	var td = FCCreateAndAdd(maindiv, 'div');
	
	// create the title
	FCCreateAndAdd(td, 'p', null, null, title);
	
	// and the containing div
	var div = FCCreateAndAdd(maindiv, 'div');
	
	var enableDisableAll = function()
	{
		var isOn = this.className == 'off' ? false : true;
		
		// switch the all status
		this.className = (isOn == true) ? 'off' : 'on';
	
		// run down the children of the div ignoring the first one
		for (var i = 1; i < div.childNodes.length; i++)
		{
			if (div.childNodes[i].nodeName == 'IMG' && SearchCommon.isSectionHeader(div.childNodes[i].className) == false)
			{
				div.childNodes[i].className = (isOn == true) ? 'off' : 'on';
			}
		}
	};
	
	var createOnClick = function(element)
	{
		return function()
		{
			element.onclick();
		}
	}
	
	var createSectionOnClick = function(elements)
	{
		return function()
		{
			if (this.className == 'section')
			{
				this.className = 'sectionopen';
				
				for (var k = 0; k < elements.length; k++)
				{
					elements[k].style.display = '';
				}
			}
			else
			{
				this.className = 'section';
				
				for (var k = 0; k < elements.length; k++)
				{
					elements[k].style.display = 'none';
				}
			}
		}
	}
	
	var createSectionEnableDisable = function(elements)
	{
		return function()
		{
			var isOn = this.className == 'off' ? false : true;
		
			// switch the all status
			this.className = (isOn == true) ? 'off' : 'on';
			
			// run down the section entries making them match the new section checkbox setting
			for (var i = 0; i < elements.length; i++)
			{
				elements[i].className = (isOn == true) ? 'off' : 'on';
			}
		}
	}
	
	var createEnableDisableCheckbox = function(sectoionCheckbox)
	{
		return function()
		{
			// 'this' will refer to the component
			if (this.className == 'off')	this.className = 'on';
			else							this.className = 'off';
			
			// now if it is on then set the parent on also
			if (this.className == 'on')
			{
				sectoionCheckbox.className = 'on';
			}
		}
	}
	
	// create the IDs using the column index as a prefix
	var colIndex = tr.childNodes.length - 1;
	
	// and the 'All' entry
	var img = FCCreateAndAdd(div, 'img', colIndex + '_(All)');
	img.src = "../images/transparent.png";
	img.width = "19";
	img.height = "19";
	img.onclick = enableDisableAll;
	FCCreateAndAdd(div, 'span', null, 'orange', '(All)').onclick = createOnClick(img);
	FCCreateAndAdd(div, 'br');
	
	for (var i = 0; i < names.length; i++)
	{
		if (values == null)
		{
			// this is a regular entry
			var img = FCCreateAndAdd(div, 'img', colIndex + '_' + names[i].split(' ').join('_'));
			img.src = "../images/transparent.png";
			img.width = "19";
			img.height = "19";
			img.onclick = enableDisableCheckbox;
			FCCreateAndAdd(div, 'span', null, null, names[i]).onclick = createOnClick(img);
			FCCreateAndAdd(div, 'br');
		}
		else
		{
			// this is a sectioned list so create the header
			var sectionCheckbox = FCCreateAndAdd(div, 'img', '_' + colIndex + '_' + names[i].split(' ').join('_'), null);
			sectionCheckbox.src = "../images/transparent.png";
			sectionCheckbox.width = "19";
			sectionCheckbox.height = "19";
			
			var sectionArrow = FCCreateAndAdd(div, 'img', null, 'section');
			sectionArrow.src = "../images/transparent.png";
			sectionArrow.width = "19";
			sectionArrow.height = "19";
			
			var secSpan = FCCreateAndAdd(div, 'span', null, null, names[i]);
			FCCreateAndAdd(div, 'br');
			
			// and all of the sub entries
			var entries = values[names[i]];
			
			var elements = new Array();
			
			for (var j = 0; j < entries.length; j++)
			{
				var subImg = FCCreateAndAdd(div, 'img', colIndex + '_' + entries[j].split(' ').join('_'));
				subImg.src = "../images/transparent.png";
				subImg.width = "19";
				subImg.height = "19";
				subImg.onclick = createEnableDisableCheckbox(sectionCheckbox);//enableDisableCheckbox;
				subImg.style.marginLeft = '25px'; // should be done via CSS ideally
				var subSpan = FCCreateAndAdd(div, 'span', null, null, entries[j]);
				var subBr = FCCreateAndAdd(div, 'br');
				
				subSpan.onclick = createOnClick(subImg);
				
				// set all of the elements so they are not displayed by default
				subImg.style.display = 'none';
				subSpan.style.display = 'none';
				subBr.style.display = 'none';
				
				elements.push(subImg);
				elements.push(subSpan);
				elements.push(subBr);
			}
			
			// turn all children on/off with the section header
			sectionCheckbox.onclick = createSectionEnableDisable(elements);
			
			// and add an event to the section header so that it shows the children when clicked and vice-versa
			sectionArrow.onclick = createSectionOnClick(elements);
			
			secSpan.onclick = createOnClick(sectionArrow);
		}
	}
	
	return maindiv;
}

SearchCommon.getSelectedCheckboxes = function(tr, index, sections) // set sections = true for the section header checkbox values, false otherwise (and get the content checkboxes instead)
{
	
	var td = tr.childNodes[index];
	
	//XBAssert(td.childNodes[0].nodeName == 'P', 'first entry should be P');
	//XBAssert(td.childNodes[1].nodeName == 'DIV', 'second entry should be DIV');
	
	var div = td.childNodes[1];
	
	var selected = new Array();
	
	var allOn = true;
	
	// iterate over the div picking out all of the enabled checkboxes (ignoring the first 'All' one)
	for (var i = 1; i < div.childNodes.length; i++)
	{
		if (div.childNodes[i].nodeName == 'IMG' && SearchCommon.isSectionHeader(div.childNodes[i].className) == false)
		{
			if (sections == true)
			{
				if (div.childNodes[i].id.charAt(0) != '_') continue; // not a section header
				
				var isOn = div.childNodes[i].className == 'off' ? false : true;
			
				if (isOn == true)
				{
					var span = div.childNodes[i + 2]; // skip the arrow
					
					//XBAssert(span.nodeName == 'SPAN', 'entry should be SPAN');
					
					var text = FCGetInnerText(span);
					
					selected.push(text);
				}
				else
				{
					allOn = false;
				}
			}
			else
			{
				if (div.childNodes[i].id.charAt(0) == '_') continue; // is a section header
				
				var isOn = div.childNodes[i].className == 'off' ? false : true;
				//console.log(isOn)
				if (isOn == true)
				{
					var span = div.childNodes[i + 1];
					
					//XBAssert(span.nodeName == 'SPAN', 'entry should be SPAN');
					
					var text = FCGetInnerText(span);
					
					selected.push(text);
				}
				else
				{
					allOn = false;
				}
			}
		}
	}
	
	// if everything is on then just return null so the list can be excluded from the query that uses it
	if (allOn == true)	return null;
	else				return selected;
}

// bit different to above, this one includes the (All) entries and never returns null
SearchCommon.getSelectedCheckboxIDs = function(tr, index)
{
	var td = tr.childNodes[index];
	
	//XBAssert(td.childNodes[0].nodeName == 'P', 'first entry should be P');
	//XBAssert(td.childNodes[1].nodeName == 'DIV', 'second entry should be DIV');
	
	var div = td.childNodes[1];
	
	var selected = new Array();
	
	// iterate over the div picking out all of the enabled checkboxes (including the first 'All' one)
	for (var i = 0; i < div.childNodes.length; i++)
	{
		if (div.childNodes[i].nodeName == 'IMG' && SearchCommon.isSectionHeader(div.childNodes[i].className) == false)
		{
			var isOn = div.childNodes[i].className == 'off' ? false : true;
			
			if (isOn == true)
			{
				var id = div.childNodes[i].id;
				
				selected.push(id);
			}
		}
	}
	
	return selected;
}

SearchCommon.setAllCheckboxes = function(state)
{
	var tr = document.getElementById('filterContent').childNodes[0];
	
	for (var i = 0; i < tr.cells.length; i++)
	{
		var td = tr.cells[i];
		
		//XBAssert(td.childNodes[0].nodeName == 'P', 'first entry should be P');
		//XBAssert(td.childNodes[1].nodeName == 'DIV', 'second entry should be DIV');
		
		var div = td.childNodes[1];
		
		// iterate over the div picking out all of the enabled checkboxes (including the first 'All' one)
		for (var j = 0; j < div.childNodes.length; j++)
		{
			if (div.childNodes[j].nodeName == 'IMG' && SearchCommon.isSectionHeader(div.childNodes[j].className) == false)
			{
				div.childNodes[j].className = (state == true ? 'on' : 'off');
			}
		}
	}
}

SearchCommon.setCheckboxGroup = function(index, state)
{
	var tr = document.getElementById('filterContent');
	
	var td = tr.childNodes[index];
		
	//XBAssert(td.childNodes[0].nodeName == 'P', 'first entry should be P');
	//XBAssert(td.childNodes[1].nodeName == 'DIV', 'second entry should be DIV');
	
	var div = td.childNodes[1];
	
	// iterate over the div picking out all of the enabled checkboxes (including the first 'All' one)
	for (var j = 0; j < div.childNodes.length; j++)
	{
		if (div.childNodes[j].nodeName == 'IMG' && SearchCommon.isSectionHeader(div.childNodes[j].className) == false)
		{
			div.childNodes[j].className = (state == true ? 'on' : 'off');
		}
	}
}

SearchCommon.setSelectedCheckboxIDs = function(selected)
{
	for (var i = 0; i < selected.length; i++)
	{
		document.getElementById(selected[i]).className = 'on';
	}
}

SearchCommon.isSectionHeader = function(className)
{
	if (className == 'section' || className == 'sectionopen')	return true;
	else														return false;
}

SearchCommon.isSelected = function(checkboxId)
{
	// 'off' is explicitly not selected, 'on' or blank is selected so we only check for off
	return !(document.getElementById(checkboxId).className == 'off');
}

SearchCommon.setSelectedCheckbox = function(checkboxLabel, index, isGroup)
{
	// the id is the display name so use the algo same as above and prefix with the index of the column
	// and replace spaces with underscores.
	// This is a section header checkbox (for dec100 it is anyway) so it needs an additional underscore
	var checkboxId = null;
	
	if (isGroup == true)	checkboxId = '_' + index + '_' + checkboxLabel.split(' ').join('_');
	else					checkboxId = 	   index + '_' + checkboxLabel.split(' ').join('_');
	
	document.getElementById(checkboxId).className = 'on';
	
	// in case it is a section header checkbox then generate clicks to get the 'switch on' logic executed
	document.getElementById(checkboxId).onclick(); // off
	document.getElementById(checkboxId).onclick(); // and on again 
}

SearchCommon.openGroup = function(index, groupLabel)
{
	var tr = document.getElementById('filterTable').childNodes[0];
	
	var td = tr.childNodes[index];
		
	//XBAssert(td.childNodes[0].nodeName == 'P', 'first entry should be P');
	//XBAssert(td.childNodes[1].nodeName == 'DIV', 'second entry should be DIV');
	
	var div = td.childNodes[1];
	
	// create the group checkbox name
	var checkboxId = '_' + index + '_' + groupLabel.split(' ').join('_');
	
	// and then grab the arrow beside it
	var groupElem = document.getElementById(checkboxId).nextSibling;

	//XBAssert(groupElem.className == 'section' || groupElem.className == 'sectionopen', 'elem is not a group');

	// if the arrow is not open then 'click it' to open it
	if (groupElem.className == 'section')
	{
		groupElem.onclick();	
	}
}

