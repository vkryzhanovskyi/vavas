//File from DEC legacy - need to refactor out, though a couple of the functions are useful. 
// foundation classes (even tho they are functions...)

function FCIsDigit(c)
{
	var cCode = c.charCodeAt(0);
	
	return (cCode >= '0'.charCodeAt(0) && cCode <= '9'.charCodeAt(0));
}

function FCAsDigit(c)
{
	var cCode = c.charCodeAt(0);
	
	return (cCode - '0'.charCodeAt(0));
}

function FCAsHex(c)
{
	var cCode = c.charCodeAt(0);
	
	switch (cCode)
	{
		case 'F'.charCodeAt(0) :	return 15;
		case 'E'.charCodeAt(0) :	return 14;
		case 'D'.charCodeAt(0) :	return 13;
		case 'C'.charCodeAt(0) :	return 12;
		case 'B'.charCodeAt(0) :	return 11;
		case 'A'.charCodeAt(0) :	return 10;
		default :					return (cCode - '0'.charCodeAt(0));
	}	
}

function FCExtractInt(str, startIndex, numDigits)
{
	var intValue = 0;
	
	for (var i = 0; i < numDigits; i++)
	{
		var c = str.charAt(startIndex + i);
		
		intValue = intValue * 10;
		intValue += FCAsDigit(c);
	}
	
	return intValue;
}

function FCExtractHex(str, startIndex, numDigits)
{
	var intValue = 0;
	
	for (var i = 0; i < numDigits; i++)
	{
		var c = str.charAt(startIndex + i);
		
		intValue = intValue * 16;
		intValue += FCAsHex(c);
	}
	
	return intValue;
}

function FCRemoveAllChildren(element)
{
	while (element.hasChildNodes())
	{
		element.removeChild(element.firstChild);
	}
}

function FCRemoveAllChildrenExcept(element, leave)
{
	for (var i = element.childNodes.length - 1; i > 0; i--)
	{
		if (element.childNodes[i].tagName != leave)
		{
			element.removeChild(element.childNodes[i]);
		}
	}
}

function FCIsUnicodePrivateUseCharacter(ch)
{
	if (ch >= '\uE000' && ch <= '\uF8FF')	return true;
	else									return false;
}

function FCGetInnerText(element)
{
	return element.textContent ? element.textContent : element.innerText;
}

function FCSetInnerText(element, text)
{
	if (typeof(element.textContent) != 'undefined')	element.textContent = text;
	else											element.innerText = text;
}

function FCInnerTextLength(element)
{
	if (typeof(element.textContent) != 'undefined')	return element.textContent.length;
	else											return element.innerText.length;
}

function FCCreateAndAdd(parentElement, type, id, className, value)
{
	var element = document.createElement(type);
	
	if (id != null && typeof(id) != 'undefined' && id.length != 0)
	{
		element.id = id;
	}
	
	if (className != null && typeof(className) != 'undefined' && className.length != 0)
	{
		element.className = className;
	}

	if (value != null && typeof(value) != 'undefined' && value.length != 0)
	{
		FCSetInnerText(element, value);
	}
	
	parentElement.appendChild(element);
	
	return element;
}