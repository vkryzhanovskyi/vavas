//company profile populator
company = function(){}
company.onLoad = function(){
    if(localStorage.companyNo){
        //get the company info from LLDB where there's a companyNo.
        var cono = localStorage.companyNo;
        LiveListDB.execSQL('SELECT * FROM Companies WHERE Company_ID=?',[cono],company.populate)
    }
}

company.populate = function(tr,co){
    if(co.rows.length>0){
        var Company = co.rows[0];
        console.log(Company)
        //add the basic company information first
        FCSetInnerText(document.getElementById('CompanyName'),Company.Company_Name)
        if(company.KnownAs!=null){FCSetInnerText(document.getElementById('KnownAs'),'(Known as '+Company.KnownAs+')')}
        FCSetInnerText(document.getElementById('Industry'),Company.Company_Industry)
        FCSetInnerText(document.getElementById('AdvSector'),Company.Company_Adv_Sector)
        FCSetInnerText(document.getElementById('Region'),Company.Company_Region)
        FCSetInnerText(document.getElementById('Location'),Company.Company_Location)
        FCSetInnerText(document.getElementById('BusDesc'),Company.Company_Business_Description)
        //then add the chart data
        company.buildRevChart(Company)
        company.buildEBITDAChart(Company)
    }
}

company.buildRevChart = function(co){
    if(co.Current_Rev==''&&co.Rev_Minus_One==''&&co.Rev_Minus_Two==''&&co.Rev_Minus_Three==''&&co.Rev_Minus_Four==''){
        console.log('setting innerText')
        document.getElementById('revChartDiv').innerHTML = '<h2 style="font-align:center">Small/offshore company - no filed accounts available.</h2>'
    } else {
    console.log(co)
    RevLabels = ['2011','2012','2013','2014','2015']
    revData = [co.Rev_Minus_Four,co.Rev_Minus_Three,co.Rev_Minus_Two,co.Rev_Minus_One,co.Current_Rev]
    var data = {
        labels: RevLabels,
        datasets: [
            {
                label:'',
                fill: false,
                lineTension: 0.1,
                backgroundColor: "rgba(75,192,192,0.4)",
                borderColor: "rgba(75,192,192,1)",
                borderCapStyle: 'butt',
                borderDash: [],
                borderDashOffset: 0.0,
                borderJoinStyle: 'miter',
                pointBorderColor: "rgba(75,192,192,1)",
                pointBackgroundColor: "#fff",
                pointBorderWidth: 1,
                pointHoverRadius: 5,
                pointHoverBackgroundColor: "rgba(75,192,192,1)",
                pointHoverBorderColor: "rgba(220,220,220,1)",
                pointHoverBorderWidth: 2,
                pointRadius: 1,
                pointHitRadius: 10,
                data: revData,
                spanGaps: false,
            }
        ]
    }
    var ctx = document.getElementById('RevChart').getContext("2d");
    var revchart = new Chart(ctx, {
        type: 'line',
        data: data,
        options: {
            legend:{
                display: false
            },
            scales: {
                yAxes: [
                    {
                        ticks: {
                            callback: function(label, index, labels) {
                            return '\u00A3'+label/1000000+'m';
                            }
                        }
                    }
                ]
            }
        }
    });}
}

company.buildEBITDAChart = function(co){
    if(co.Current_EBITDA==''&&co.EBITDA_Minus_One==''&&co.EBITDA_Minus_Two==''&&co.EBITDA_Minus_Three==''&&co.EBITDA_Minus_Four==''){
        console.log('setting innertext')
        document.getElementById('EBITDAChartDiv').innerHTML = '<h2 style="font-align:center">Small/offshore company - no filed accounts available.</h2>'
    } else {
    console.log(co)
    EBITDALabels = ['2011','2012','2013','2014','2015']
    EBITDAData = [co.EBITDA_Minus_Four,co.EBITDA_Minus_Three,co.EBITDA_Minus_Two,co.EBITDA_Minus_One,co.Current_EBITDA]
    var data = {
        labels: EBITDALabels,
        datasets: [
            {
                label:'EBITDA',
                fill: false,
                lineTension: 0.1,
                backgroundColor: "rgba(75,192,192,0.4)",
                borderColor: "rgba(75,192,192,1)",
                borderCapStyle: 'butt',
                borderDash: [],
                borderDashOffset: 0.0,
                borderJoinStyle: 'miter',
                pointBorderColor: "rgba(75,192,192,1)",
                pointBackgroundColor: "#fff",
                pointBorderWidth: 1,
                pointHoverRadius: 5,
                pointHoverBackgroundColor: "rgba(75,192,192,1)",
                pointHoverBorderColor: "rgba(220,220,220,1)",
                pointHoverBorderWidth: 2,
                pointRadius: 1,
                pointHitRadius: 10,
                data: EBITDAData,
                spanGaps: false,
            }
        ]
    }
    var ctx = document.getElementById('EBITDAChart').getContext("2d");
    var EBITDAchart = new Chart(ctx, {
        type: 'line',
        data: data,
        options: {
            legend:{
                display: false
            },
            scales: {
                yAxes: [
                    {
                        ticks: {
                            callback: function(label, index, labels) {
                            return '\u00A3'+label/1000000+'m';
                            }
                        }
                    }
                ]
            }
            
        }
    });
    }
}

