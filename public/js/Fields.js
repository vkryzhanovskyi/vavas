Fields = function(){}
//this contains the structure and options for the various fields in the database; and therefore the filters that should apply.
//TRANSACTIONS and UK_REGIONS are legacy and should be ignored - everything else is good though.
Fields.getRegion = function(Loc){
	return Fields.Locations_Keys.find(function(Region){return Fields.Locations[Region].indexOf(Loc)>-1})
}
Fields.getSector = function(Ind){
	return Fields.Advanced_Sector_Keys.find(function(Sector){return Fields.Advanced_Sector[Sector].indexOf(Ind)>-1})
}

/////////////Location//////////////////////////

Fields.Regions = new Array();
Fields.Regions.push('Central Ireland');
Fields.Regions.push('Northern Ireland');
Fields.Regions.push('Dublin');
Fields.Regions.push('Eastern Ireland');
Fields.Regions.push('East Midlands');
Fields.Regions.push('Eastern');
Fields.Regions.push('London');
Fields.Regions.push('North East');
Fields.Regions.push('North West & Merseyside');
Fields.Regions.push('Scotland');
Fields.Regions.push('South East');
Fields.Regions.push('South West');
Fields.Regions.push('Wales');
Fields.Regions.push('West Midlands');
Fields.Regions.push('Yorkshire & The Humber');
Fields.Regions.push('APAC');
Fields.Regions.push('Australia');
Fields.Regions.push('Africa');
Fields.Regions.push('Middle East');
Fields.Regions.push('Eastern Europe');
Fields.Regions.push('USA');
Fields.Regions.push('Canada');
Fields.Regions.push('LatAm');
Fields.Regions.push('South America');
Fields.Regions.push('Western Europe');

Fields.UK_REGIONS = new Array();
Fields.UK_REGIONS.push('Northern Ireland');
Fields.UK_REGIONS.push('East Midlands');
Fields.UK_REGIONS.push('Eastern');
Fields.UK_REGIONS.push('London');
Fields.UK_REGIONS.push('North East');
Fields.UK_REGIONS.push('North West & Merseyside');
Fields.UK_REGIONS.push('Scotland');
Fields.UK_REGIONS.push('South East');
Fields.UK_REGIONS.push('South West');
Fields.UK_REGIONS.push('Wales');
Fields.UK_REGIONS.push('West Midlands');
Fields.UK_REGIONS.push('Yorkshire & The Humber');

Fields.Locations_Keys = new Array();
Fields.Locations = new Object();

for (var i = 0; i < Fields.Regions.length; i++)
{
	var region = Fields.Regions[i];
	
	Fields.Locations[region] = new Array();
}

Fields.Locations_Keys.push('Central Ireland');
Fields.Locations['Central Ireland'].push('Clare');
Fields.Locations['Central Ireland'].push('Cork');
Fields.Locations['Central Ireland'].push('Dunleer');
Fields.Locations['Central Ireland'].push('Galway');
Fields.Locations['Central Ireland'].push('Limerick');
Fields.Locations['Central Ireland'].push('Mayo');
Fields.Locations['Central Ireland'].push('Sligo');
Fields.Locations['Central Ireland'].push('Tralee');
Fields.Locations['Central Ireland'].push('Cavan');
Fields.Locations['Central Ireland'].push('County Fermanagh');
Fields.Locations['Central Ireland'].push('Wexford');

Fields.Locations_Keys.push('Northern Ireland');
Fields.Locations['Northern Ireland'].push('Antrim');
Fields.Locations['Northern Ireland'].push('Belfast');
Fields.Locations['Northern Ireland'].push('Dunmurray');
Fields.Locations['Northern Ireland'].push('Lisburn');

Fields.Locations_Keys.push('Dublin');
Fields.Locations['Dublin'].push('Dublin');
Fields.Locations['Dublin'].push('Kildare');

Fields.Locations_Keys.push('Eastern Ireland');
Fields.Locations['Eastern Ireland'].push('Carlow');
Fields.Locations['Eastern Ireland'].push('Kilkenny');
Fields.Locations['Eastern Ireland'].push('Waterford');
Fields.Locations['Eastern Ireland'].push('Wicklow');

Fields.Locations_Keys.push('East Midlands');
Fields.Locations['East Midlands'].push('Alfreton');
Fields.Locations['East Midlands'].push('Ashby');
Fields.Locations['East Midlands'].push('Belper');
Fields.Locations['East Midlands'].push('Boston');
Fields.Locations['East Midlands'].push('Bourne');
Fields.Locations['East Midlands'].push('Breedon-on-the-Hill');
Fields.Locations['East Midlands'].push('Castle Donnington');
Fields.Locations['East Midlands'].push('Chesterfield');
Fields.Locations['East Midlands'].push('Coalville');
Fields.Locations['East Midlands'].push('Corby');
Fields.Locations['East Midlands'].push('Crowle');
Fields.Locations['East Midlands'].push('Derby');
Fields.Locations['East Midlands'].push('Derbyshire');
Fields.Locations['East Midlands'].push('Hinckley');
Fields.Locations['East Midlands'].push('Hucknall');
Fields.Locations['East Midlands'].push('Ilkeston');
Fields.Locations['East Midlands'].push('Kettering');
Fields.Locations['East Midlands'].push('Leicester');
Fields.Locations['East Midlands'].push('Leicestershire');
Fields.Locations['East Midlands'].push('Lincoln');
Fields.Locations['East Midlands'].push('Lincolnshire');
Fields.Locations['East Midlands'].push('Loughborough');
Fields.Locations['East Midlands'].push('Mansfield');
Fields.Locations['East Midlands'].push('Market Harborough');
Fields.Locations['East Midlands'].push('Meriden');
Fields.Locations['East Midlands'].push('Northampton');
Fields.Locations['East Midlands'].push('Northamptonshire');
Fields.Locations['East Midlands'].push('Nottingham');
Fields.Locations['East Midlands'].push('Spalding');
Fields.Locations['East Midlands'].push('Wellingborough');
Fields.Locations['East Midlands'].push('Woodbridge');

Fields.Locations_Keys.push('Eastern');
Fields.Locations['Eastern'].push('Basildon');
Fields.Locations['Eastern'].push('Bedford');
Fields.Locations['Eastern'].push('Bedfordshire');
Fields.Locations['Eastern'].push('Borehamwood');
Fields.Locations['Eastern'].push('Brentford');
Fields.Locations['Eastern'].push('Brentwood');
Fields.Locations['Eastern'].push('Cambridge');
Fields.Locations['Eastern'].push('Cambridgeshire');
Fields.Locations['Eastern'].push('Chelmsford');
Fields.Locations['Eastern'].push('Cheshunt');
Fields.Locations['Eastern'].push('Colchester');
Fields.Locations['Eastern'].push('Cranfield');
Fields.Locations['Eastern'].push('Essex');
Fields.Locations['Eastern'].push('Harlow');
Fields.Locations['Eastern'].push('Haverhill');
Fields.Locations['Eastern'].push('Hemel Hempstead');
Fields.Locations['Eastern'].push('Hertfordshire');
Fields.Locations['Eastern'].push('Kings Lynn');
Fields.Locations['Eastern'].push('Letchworth');
Fields.Locations['Eastern'].push('Loddon');
Fields.Locations['Eastern'].push('Lowestoft');
Fields.Locations['Eastern'].push('Luton');
Fields.Locations['Eastern'].push('Newark');
Fields.Locations['Eastern'].push('Norwich');
Fields.Locations['Eastern'].push('Oundle');
Fields.Locations['Eastern'].push('Peterborough');
Fields.Locations['Eastern'].push('Rayleigh');
Fields.Locations['Eastern'].push('Rickmansworth');
Fields.Locations['Eastern'].push('Romford');
Fields.Locations['Eastern'].push('Royston');
Fields.Locations['Eastern'].push('Sawbridgeworth');
Fields.Locations['Eastern'].push('St Albans');
Fields.Locations['Eastern'].push('St Neots');
Fields.Locations['Eastern'].push('Suffolk');
Fields.Locations['Eastern'].push('Thetford');
Fields.Locations['Eastern'].push('Watford');
Fields.Locations['Eastern'].push('Welwyn Garden City');
Fields.Locations['Eastern'].push('Yarmouth');

Fields.Locations_Keys.push('London');
Fields.Locations['London'].push('Battersea');
Fields.Locations['London'].push('Chessington');
Fields.Locations['London'].push('City of London');
Fields.Locations['London'].push('Croydon');
Fields.Locations['London'].push('Fulham');
Fields.Locations['London'].push('Hainault');
Fields.Locations['London'].push('Hampton');
Fields.Locations['London'].push('Harrow');
Fields.Locations['London'].push('Heathrow');
Fields.Locations['London'].push('Kingston-upon-Thames');
Fields.Locations['London'].push('London');
Fields.Locations['London'].push('Mayfair');
Fields.Locations['London'].push('Middlesex');
Fields.Locations['London'].push('Richmond-upon-Thames');
Fields.Locations['London'].push('Stratford-upon-Avon');
Fields.Locations['London'].push('Tiptree');
Fields.Locations['London'].push('Twickenham');
Fields.Locations['London'].push('Uxbridge');
Fields.Locations['London'].push('Wembley');
Fields.Locations['London'].push('Wimbledon');

Fields.Locations_Keys.push('North East');
Fields.Locations['North East'].push('Alnwick');
Fields.Locations['North East'].push('Billingham');
Fields.Locations['North East'].push('Blyth');
Fields.Locations['North East'].push('County Durham');
Fields.Locations['North East'].push('Cramlington');
Fields.Locations['North East'].push('Cumbria');
Fields.Locations['North East'].push('Darlington');
Fields.Locations['North East'].push('Durham');
Fields.Locations['North East'].push('Gateshead');
Fields.Locations['North East'].push('Hartlepool');
Fields.Locations['North East'].push('Hebburn');
Fields.Locations['North East'].push('Hexham');
Fields.Locations['North East'].push('Houghton-le-Spring');
Fields.Locations['North East'].push('Jarrow');
Fields.Locations['North East'].push('Killingworth');
Fields.Locations['North East'].push('Middlesbrough');
Fields.Locations['North East'].push('Morpeth');
Fields.Locations['North East'].push('Newcastle-upon-Tyne');
Fields.Locations['North East'].push('Newton Aycliffe');
Fields.Locations['North East'].push('North Shields');
Fields.Locations['North East'].push('Northallerton');
Fields.Locations['North East'].push('Peterlee');
Fields.Locations['North East'].push('Redcar');
Fields.Locations['North East'].push('Sedgefield');
Fields.Locations['North East'].push('South Shields');
Fields.Locations['North East'].push('Stockton-on-Tees');
Fields.Locations['North East'].push('Sunderland');

Fields.Locations_Keys.push('North West & Merseyside');
Fields.Locations['North West & Merseyside'].push('Altrincham');
Fields.Locations['North West & Merseyside'].push('Birkenhead');
Fields.Locations['North West & Merseyside'].push('Blackburn');
Fields.Locations['North West & Merseyside'].push('Blackpool');
Fields.Locations['North West & Merseyside'].push('Bolton');
Fields.Locations['North West & Merseyside'].push('Bredbury');
Fields.Locations['North West & Merseyside'].push('Burnley');
Fields.Locations['North West & Merseyside'].push('Bury');
Fields.Locations['North West & Merseyside'].push('Carlisle');
Fields.Locations['North West & Merseyside'].push('Cheadle');
Fields.Locations['North West & Merseyside'].push('Cheshire');
Fields.Locations['North West & Merseyside'].push('Chester');
Fields.Locations['North West & Merseyside'].push('Chorley');
Fields.Locations['North West & Merseyside'].push('Colne');
Fields.Locations['North West & Merseyside'].push('Crewe');
Fields.Locations['North West & Merseyside'].push('Daresbury');
Fields.Locations['North West & Merseyside'].push('Haydock');
Fields.Locations['North West & Merseyside'].push('Isle of Man');
Fields.Locations['North West & Merseyside'].push('Kirkby');
Fields.Locations['North West & Merseyside'].push('Knowsley');
Fields.Locations['North West & Merseyside'].push('Knutsford');
Fields.Locations['North West & Merseyside'].push('Lancashire');
Fields.Locations['North West & Merseyside'].push('Lancaster');
Fields.Locations['North West & Merseyside'].push('Leigh');
Fields.Locations['North West & Merseyside'].push('Leyland');
Fields.Locations['North West & Merseyside'].push('Liverpool');
Fields.Locations['North West & Merseyside'].push('Lytham St Annes');
Fields.Locations['North West & Merseyside'].push('Macclesfield');
Fields.Locations['North West & Merseyside'].push('Manchester');
Fields.Locations['North West & Merseyside'].push('Merseyside');
Fields.Locations['North West & Merseyside'].push('Northwich');
Fields.Locations['North West & Merseyside'].push('Oldham');
Fields.Locations['North West & Merseyside'].push('Preston');
Fields.Locations['North West & Merseyside'].push('Runcorn');
Fields.Locations['North West & Merseyside'].push('Sale');
Fields.Locations['North West & Merseyside'].push('Salford');
Fields.Locations['North West & Merseyside'].push('Skelmersdale');
Fields.Locations['North West & Merseyside'].push('St Helens');
Fields.Locations['North West & Merseyside'].push('Stockport');
Fields.Locations['North West & Merseyside'].push('Warrington');
Fields.Locations['North West & Merseyside'].push('Wigan');
Fields.Locations['North West & Merseyside'].push('Wigton');
Fields.Locations['North West & Merseyside'].push('Wilmslow');
Fields.Locations['North West & Merseyside'].push('Winsford');
Fields.Locations['North West & Merseyside'].push('Wirral');

Fields.Locations_Keys.push('Scotland');
Fields.Locations['Scotland'].push('Aberdeen');
Fields.Locations['Scotland'].push('Alexandria');
Fields.Locations['Scotland'].push('Alloa');
Fields.Locations['Scotland'].push('Ayr');
Fields.Locations['Scotland'].push('Bathgate');
Fields.Locations['Scotland'].push('Bellshill');
Fields.Locations['Scotland'].push('Campbeltown');
Fields.Locations['Scotland'].push('Cumbernault');
Fields.Locations['Scotland'].push('Deeside');
Fields.Locations['Scotland'].push('Dumbarton');
Fields.Locations['Scotland'].push('Dumfries');
Fields.Locations['Scotland'].push('Dunbar');
Fields.Locations['Scotland'].push('Dundee');
Fields.Locations['Scotland'].push('Dunfermline');
Fields.Locations['Scotland'].push('Duns');
Fields.Locations['Scotland'].push('East Kilbride');
Fields.Locations['Scotland'].push('Edinburgh');
Fields.Locations['Scotland'].push('Ellon');
Fields.Locations['Scotland'].push('Fife');
Fields.Locations['Scotland'].push('Forfar');
Fields.Locations['Scotland'].push('Forres');
Fields.Locations['Scotland'].push('Forth');
Fields.Locations['Scotland'].push('Glasgow');
Fields.Locations['Scotland'].push('Inverness');
Fields.Locations['Scotland'].push('Lanarkshire');
Fields.Locations['Scotland'].push('Linlithgow');
Fields.Locations['Scotland'].push('Livingston');
Fields.Locations['Scotland'].push('Livingstone');
Fields.Locations['Scotland'].push('Lochaber');
Fields.Locations['Scotland'].push('Midlothian');
Fields.Locations['Scotland'].push('Mintlaw');
Fields.Locations['Scotland'].push('Motherwell');
Fields.Locations['Scotland'].push('Musselburgh');
Fields.Locations['Scotland'].push('Newbridge');
Fields.Locations['Scotland'].push('Oban');
Fields.Locations['Scotland'].push('Paisley');
Fields.Locations['Scotland'].push('Perth');
Fields.Locations['Scotland'].push('Portlethen');
Fields.Locations['Scotland'].push('Renfrewshire');
Fields.Locations['Scotland'].push('Roslin');
Fields.Locations['Scotland'].push('Rosyth');
Fields.Locations['Scotland'].push('Roxburghshire');
Fields.Locations['Scotland'].push('St Andrews');
Fields.Locations['Scotland'].push('St Boswells');
Fields.Locations['Scotland'].push('Stirling');
Fields.Locations['Scotland'].push('Strathaven');

Fields.Locations_Keys.push('South East');
Fields.Locations['South East'].push('Abertillery');
Fields.Locations['South East'].push('Abingdon');
Fields.Locations['South East'].push('Amersham');
Fields.Locations['South East'].push('Andover');
Fields.Locations['South East'].push('Apsley');
Fields.Locations['South East'].push('Ascot');
Fields.Locations['South East'].push('Ashford');
Fields.Locations['South East'].push('Aylesbury');
Fields.Locations['South East'].push('Basingstoke');
Fields.Locations['South East'].push('Beaconsfield');
Fields.Locations['South East'].push('Berkshire');
Fields.Locations['South East'].push('Bicester');
Fields.Locations['South East'].push('Bracknell');
Fields.Locations['South East'].push('Brighton');
Fields.Locations['South East'].push('Bromley');
Fields.Locations['South East'].push('Brookwood');
Fields.Locations['South East'].push('Buckingham');
Fields.Locations['South East'].push('Buckinghamshire');
Fields.Locations['South East'].push('Camberley');
Fields.Locations['South East'].push('Canterbury');
Fields.Locations['South East'].push('Carshalton');
Fields.Locations['South East'].push('Caterham');
Fields.Locations['South East'].push('Chertsey');
Fields.Locations['South East'].push('Chichester');
Fields.Locations['South East'].push('Chiswick');
Fields.Locations['South East'].push('Cobham');
Fields.Locations['South East'].push('Crawley');
Fields.Locations['South East'].push('Datchet');
Fields.Locations['South East'].push('Didcot');
Fields.Locations['South East'].push('Dover');
Fields.Locations['South East'].push('East Sussex');
Fields.Locations['South East'].push('Eastbourne');
Fields.Locations['South East'].push('Egham');
Fields.Locations['South East'].push('Epsom');
Fields.Locations['South East'].push('Farnborough');
Fields.Locations['South East'].push('Farnham');
Fields.Locations['South East'].push('Frimley');
Fields.Locations['South East'].push('Godalming');
Fields.Locations['South East'].push('Gosport');
Fields.Locations['South East'].push('Guildford');
Fields.Locations['South East'].push('Haddenham');
Fields.Locations['South East'].push('Hailsham');
Fields.Locations['South East'].push('Hampshire');
Fields.Locations['South East'].push('Hatfield');
Fields.Locations['South East'].push('Havant');
Fields.Locations['South East'].push('Henley-on-Thames');
Fields.Locations['South East'].push('High Wycombe');
Fields.Locations['South East'].push('Hook');
Fields.Locations['South East'].push('Horsham');
Fields.Locations['South East'].push('Hungerford');
Fields.Locations['South East'].push('Isle of Wight');
Fields.Locations['South East'].push('Kent');
Fields.Locations['South East'].push('Leatherhead');
Fields.Locations['South East'].push('Leighton Buzzard');
Fields.Locations['South East'].push('Long Crendon');
Fields.Locations['South East'].push('Long Wittenham');
Fields.Locations['South East'].push('Maidenhead');
Fields.Locations['South East'].push('Maidstone');
Fields.Locations['South East'].push('Marlow');
Fields.Locations['South East'].push('Milton Keynes');
Fields.Locations['South East'].push('Mitcham');
Fields.Locations['South East'].push('New Milton');
Fields.Locations['South East'].push('Newbury');
Fields.Locations['South East'].push('Newport Pagnell');
Fields.Locations['South East'].push('Orpington');
Fields.Locations['South East'].push('Oxford');
Fields.Locations['South East'].push('Oxfordshire');
Fields.Locations['South East'].push('Oxon');
Fields.Locations['South East'].push('Portslade');
Fields.Locations['South East'].push('Portsmouth');
Fields.Locations['South East'].push('Reading');
Fields.Locations['South East'].push('Redhill');
Fields.Locations['South East'].push('Reigate');
Fields.Locations['South East'].push('Ringwood');
Fields.Locations['South East'].push('Ripley');
Fields.Locations['South East'].push('Sandwich');
Fields.Locations['South East'].push('Sevenoaks');
Fields.Locations['South East'].push('Sittingbourne');
Fields.Locations['South East'].push('Slough');
Fields.Locations['South East'].push('Southampton');
Fields.Locations['South East'].push('Staines');
Fields.Locations['South East'].push('Surrey');
Fields.Locations['South East'].push('Thame');
Fields.Locations['South East'].push('Theale');
Fields.Locations['South East'].push('Thornton Heath');
Fields.Locations['South East'].push('Tunbridge Wells');
Fields.Locations['South East'].push('Uckfield');
Fields.Locations['South East'].push('Wallingford');
Fields.Locations['South East'].push('Walton-on-Thames');
Fields.Locations['South East'].push('West Malling');
Fields.Locations['South East'].push('West Molesey');
Fields.Locations['South East'].push('Winchester');
Fields.Locations['South East'].push('Winnersh');
Fields.Locations['South East'].push('Witney');
Fields.Locations['South East'].push('Wokingham');
Fields.Locations['South East'].push('Worthing');
Fields.Locations['South East'].push('Yateley');

Fields.Locations_Keys.push('South West');
Fields.Locations['South West'].push('Ayrshire');
Fields.Locations['South West'].push('Barnstaple');
Fields.Locations['South West'].push('Bath');
Fields.Locations['South West'].push('Bournemouth');
Fields.Locations['South West'].push('Bourton on the Water');
Fields.Locations['South West'].push('Bristol');
Fields.Locations['South West'].push('Cheltenham');
Fields.Locations['South West'].push('Chippenham');
Fields.Locations['South West'].push('Cirencester');
Fields.Locations['South West'].push('Clevedon');
Fields.Locations['South West'].push('Cornwall');
Fields.Locations['South West'].push('Devizes');
Fields.Locations['South West'].push('Devon');
Fields.Locations['South West'].push('Dorchester');
Fields.Locations['South West'].push('Dorset');
Fields.Locations['South West'].push('Exeter');
Fields.Locations['South West'].push('Ferndown');
Fields.Locations['South West'].push('Gibraltar');
Fields.Locations['South West'].push('Gloucester');
Fields.Locations['South West'].push('Gloucestershire');
Fields.Locations['South West'].push('Guernsey');
Fields.Locations['South West'].push('Helston');
Fields.Locations['South West'].push('Jersey');
Fields.Locations['South West'].push('Malmesbury');
Fields.Locations['South West'].push('Newquay');
Fields.Locations['South West'].push('Plymouth');
Fields.Locations['South West'].push('Poole');
Fields.Locations['South West'].push('Salisbury');
Fields.Locations['South West'].push('Somerset');
Fields.Locations['South West'].push('Swindon');
Fields.Locations['South West'].push('Taunton');
Fields.Locations['South West'].push('Tewkesbury');
Fields.Locations['South West'].push('Wiltshire');
Fields.Locations['South West'].push('Wimborne');

Fields.Locations_Keys.push('Wales');
Fields.Locations['Wales'].push('Abercynon');
Fields.Locations['Wales'].push('Barry');
Fields.Locations['Wales'].push('Blackwood');
Fields.Locations['Wales'].push('Bridgend');
Fields.Locations['Wales'].push('Caerphilly');
Fields.Locations['Wales'].push('Cardiff');
Fields.Locations['Wales'].push('Carmarthenshire');
Fields.Locations['Wales'].push('Chepstow');
Fields.Locations['Wales'].push('Crickhowell');
Fields.Locations['Wales'].push('Cwmbran');
Fields.Locations['Wales'].push('Denbighshire');
Fields.Locations['Wales'].push('Flint');
Fields.Locations['Wales'].push('Flintshire');
Fields.Locations['Wales'].push('Gwent');
Fields.Locations['Wales'].push('Llanelli');
Fields.Locations['Wales'].push('Mold');
Fields.Locations['Wales'].push('Newport');
Fields.Locations['Wales'].push('Port Talbot');
Fields.Locations['Wales'].push('Porthmadog');
Fields.Locations['Wales'].push('Powys');
Fields.Locations['Wales'].push('St Asaph');
Fields.Locations['Wales'].push('Swansea');
Fields.Locations['Wales'].push('Usk');

Fields.Locations_Keys.push('West Midlands');
Fields.Locations['West Midlands'].push('Alcester');
Fields.Locations['West Midlands'].push('Aldridge');
Fields.Locations['West Midlands'].push('Birmingham');
Fields.Locations['West Midlands'].push('Burton upon Trent');
Fields.Locations['West Midlands'].push('Burton-on-Trent');
Fields.Locations['West Midlands'].push('Cannock');
Fields.Locations['West Midlands'].push('Coventry');
Fields.Locations['West Midlands'].push('Earls Croome');
Fields.Locations['West Midlands'].push('Hereford');
Fields.Locations['West Midlands'].push('Keele');
Fields.Locations['West Midlands'].push('Kidderminster');
Fields.Locations['West Midlands'].push('Leamington Spa');
Fields.Locations['West Midlands'].push('Leominster');
Fields.Locations['West Midlands'].push('Lichfield');
Fields.Locations['West Midlands'].push('Litchfield');
Fields.Locations['West Midlands'].push('Malvern');
Fields.Locations['West Midlands'].push('Newcastle-under-Lyme');
Fields.Locations['West Midlands'].push('Oldbury');
Fields.Locations['West Midlands'].push('Redditch');
Fields.Locations['West Midlands'].push('Rugby');
Fields.Locations['West Midlands'].push('Sandbach');
Fields.Locations['West Midlands'].push('Shropshire');
Fields.Locations['West Midlands'].push('Smethwick');
Fields.Locations['West Midlands'].push('Solihull');
Fields.Locations['West Midlands'].push('Stafford');
Fields.Locations['West Midlands'].push('Staffordshire');
Fields.Locations['West Midlands'].push('Stockton');
Fields.Locations['West Midlands'].push('Stoke-on-Trent');
Fields.Locations['West Midlands'].push('Stone');
Fields.Locations['West Midlands'].push('Sutton Coldfield');
Fields.Locations['West Midlands'].push('Tamworth');
Fields.Locations['West Midlands'].push('Telford');
Fields.Locations['West Midlands'].push('Walsall');
Fields.Locations['West Midlands'].push('Warwick');
Fields.Locations['West Midlands'].push('Warwickshire');
Fields.Locations['West Midlands'].push('Wednesbury');
Fields.Locations['West Midlands'].push('Willenhall');
Fields.Locations['West Midlands'].push('Wolverhampton');
Fields.Locations['West Midlands'].push('Worcester');

Fields.Locations_Keys.push('Yorkshire & The Humber');
Fields.Locations['Yorkshire & The Humber'].push('Birstall');
Fields.Locations['Yorkshire & The Humber'].push('Barnsley');
Fields.Locations['Yorkshire & The Humber'].push('Beverley');
Fields.Locations['Yorkshire & The Humber'].push('Bradford');
Fields.Locations['Yorkshire & The Humber'].push('Castleford');
Fields.Locations['Yorkshire & The Humber'].push('Cleckheaton');
Fields.Locations['Yorkshire & The Humber'].push('Doncaster');
Fields.Locations['Yorkshire & The Humber'].push('Halifax');
Fields.Locations['Yorkshire & The Humber'].push('Harrogate');
Fields.Locations['Yorkshire & The Humber'].push('Huddersfield');
Fields.Locations['Yorkshire & The Humber'].push('Hull');
Fields.Locations['Yorkshire & The Humber'].push('Keighley');
Fields.Locations['Yorkshire & The Humber'].push('Leeds');
Fields.Locations['Yorkshire & The Humber'].push('Mexborough');
Fields.Locations['Yorkshire & The Humber'].push('Normanton');
Fields.Locations['Yorkshire & The Humber'].push('North Yorkshire');
Fields.Locations['Yorkshire & The Humber'].push('Otley');
Fields.Locations['Yorkshire & The Humber'].push('Pontefract');
Fields.Locations['Yorkshire & The Humber'].push('Ripponden');
Fields.Locations['Yorkshire & The Humber'].push('Rotherham');
Fields.Locations['Yorkshire & The Humber'].push('Scarborough');
Fields.Locations['Yorkshire & The Humber'].push('Sheffield');
Fields.Locations['Yorkshire & The Humber'].push('Skipton');
Fields.Locations['Yorkshire & The Humber'].push('Wakefield');
Fields.Locations['Yorkshire & The Humber'].push('West Yorkshire');
Fields.Locations['Yorkshire & The Humber'].push('Wetherby');
Fields.Locations['Yorkshire & The Humber'].push('York');

Fields.Locations_Keys.push('APAC');
Fields.Locations['APAC'].push('South Asia');
Fields.Locations['APAC'].push('Central Asia');
Fields.Locations['APAC'].push('East Asia');
Fields.Locations['APAC'].push('North Asia');
Fields.Locations['APAC'].push('South East Asia');
Fields.Locations['APAC'].push('Oceania');

Fields.Locations_Keys.push('Australia');
Fields.Locations['Australia'].push('Victoria');
Fields.Locations['Australia'].push('NSW');
Fields.Locations['Australia'].push('Queensland');
Fields.Locations['Australia'].push('South Australia');
Fields.Locations['Australia'].push('Northern Territory');
Fields.Locations['Australia'].push('Western Australia');

Fields.Locations_Keys.push('Africa');
Fields.Locations['Africa'].push('North Africa');
Fields.Locations['Africa'].push('West Africa');
Fields.Locations['Africa'].push('East Africa');
Fields.Locations['Africa'].push('Central Africa');
Fields.Locations['Africa'].push('Southern Africa');

Fields.Locations_Keys.push('Middle East');
Fields.Locations['Middle East'].push('Arabian Peninsular');
Fields.Locations['Middle East'].push('Egypt');
Fields.Locations['Middle East'].push('Israel');
Fields.Locations['Middle East'].push('Levant');
Fields.Locations['Middle East'].push('Iran');
Fields.Locations['Middle East'].push('Turkey');

Fields.Locations_Keys.push('Eastern Europe');
Fields.Locations['Eastern Europe'].push('CIS');
Fields.Locations['Eastern Europe'].push('Balkans');
Fields.Locations['Eastern Europe'].push('Baltics');
Fields.Locations['Eastern Europe'].push('Central Europe');

Fields.Locations_Keys.push('USA');
Fields.Locations['USA'].push('North East');
Fields.Locations['USA'].push('Southern States');
Fields.Locations['USA'].push('South West');
Fields.Locations['USA'].push('Mid West');
Fields.Locations['USA'].push('Western Seaboard');

Fields.Locations_Keys.push('Canada');
Fields.Locations['Canada'].push('Western');
Fields.Locations['Canada'].push('Eastern');
Fields.Locations['Canada'].push('Central');

Fields.Locations_Keys.push('LatAm');
Fields.Locations['LatAm'].push('Mexico');
Fields.Locations['LatAm'].push('Central America');
Fields.Locations['LatAm'].push('Caribbean');

Fields.Locations_Keys.push('South America');
Fields.Locations['South America'].push('North');
Fields.Locations['South America'].push('West');
Fields.Locations['South America'].push('Brazil');
Fields.Locations['South America'].push('Southern Cone');

Fields.Locations_Keys.push('Western Europe');
Fields.Locations['Western Europe'].push('BENELUX');
Fields.Locations['Western Europe'].push('DACH');
Fields.Locations['Western Europe'].push('Mediterranean');
Fields.Locations['Western Europe'].push('Scandenavia');

Fields.TRANSACTIONS = new Array();
Fields.TRANSACTIONS.push('B2B');
Fields.TRANSACTIONS.push('B2C');
Fields.TRANSACTIONS.push('B2G');
Fields.TRANSACTIONS.push('B2B + B2C');
Fields.TRANSACTIONS.push('B2B + B2G');
Fields.TRANSACTIONS.push('B2C + B2G');
Fields.TRANSACTIONS.push('B2B + B2C + B2G');


/////////////////////////////////////////// Sector ///////////////////////////////////////////

Fields.Sector = ['Consumer Goods','Consumer Services','Energy','Financials','Health Care','Industrial Goods','Support Services','Materials','Technology & Telecoms','Utilities']
Fields.Advanced_Sector_Keys = ['Consumer Goods','Consumer Services','Energy','Financials','Health Care','Industrial Goods','Support Services','Materials','Technology & Telecoms','Utilities']
Fields.Advanced_Sector = new Object()
Fields.Advanced_Sector['Consumer Goods']=['Automobiles & Parts','Beverages','Food Producers','Househod Goods','Leisure Goods','Personal Goods','Tobacco']
Fields.Advanced_Sector['Consumer Services']=['Food & Drug Retailers','Goods Retailers','Media','Other Consumer Services','Travel & Leisure']
Fields.Advanced_Sector['Energy']=['Alternative Fuels','Exploration & Production','Integrated Oil & Gas','Oil Equipment & Services','Pipelines','Renewable Energy Equipment']
Fields.Advanced_Sector['Financials']=['Banks','Equity Investment Instruments','Financial Services','FinTech','Life Insurance','Nonequity Investment Instruments','Nonlife Insurance','Real Estate Investment & Services']
Fields.Advanced_Sector['Health Care']=['Biotechnology & Life Sciences','Health Care Providers','Medical Equipment','Medical Suppliers','Pharmaceuticals','Social Care Providers']
Fields.Advanced_Sector['Industrial Goods']=['Aerospace & Defence','Building Products & Materials','Electronic & Electrical Equipment','General Industrials','Home Construction','Industrial Engineering','Industrial Transportation']
Fields.Advanced_Sector['Support Services']=['Asset Storage & Management','Business Software & Services','Claims Management','Fleet Management','Hard FM','HR & Back Office Outsourcing','Media Services','Other Support Services','Professional Services','Recruitment & Training','Soft FM','Supply Chain & Logistics','TIC']
Fields.Advanced_Sector['Materials']=['Chemicals','Containers & Packaging','Industrial Metals & Mining','Other Materials','Papers & Forestry']
Fields.Advanced_Sector['Technology & Telecoms']=['Computer Hardware','Fixed Line Telecommunications','Internet & Software','IT Services','Mobile Telecommunications','Semiconductors']
Fields.Advanced_Sector['Utilities']=['Electricity','Gas','Independent Producers & Traders','Multi-Utilities','Water']


/////////////////////////////////////////// Gender ///////////////////////////////////////////

Fields.Gender = new Array();
Fields.Gender.push('Male');
Fields.Gender.push('Female');

Fields.Checklist = ['Chair','Remuneration','Nomination','Audit','SID','NED','CSR']


