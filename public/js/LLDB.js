LiveListDB = function(){};

//LLDB is a (poorly named) WebSQL system for internal data storage. This will need to move as webSQL is deprecated; but only once there is a good SQL alternative with good sizing.
LiveListDB.openDB = function(){var db = openDatabase('N100', '1.0', 'NUMIS100 Dataset', 10 * 1024 * 1024);if(db){return db} else{window.alert('Error: no db')}}

//handler - execute a websql query string on an array of data and call a function on completion.
LiveListDB.execSQL = function(qry,arr,oncomplete){
    db = LiveListDB.openDB()
    console.log(qry)
    db.transaction(function(tx){
        tx.executeSql(qry,arr,function(transaction,results){oncomplete(transaction,results)},function(transaction,error){LiveListDB.log(error)})
    })
}
//handler for batch SQL completion - useful on bulk import. Unfortunately async kills the completion timings; but prettiness of updaters can be deferred.
LiveListDB.execSQLBatch = function(qry,arr,oncomplete,table){
    db = LiveListDB.openDB()
    LiveListDB.log(qry)
    var count = (qry.match(/\?/g) || []).length;
    var completed = 0;
    var params = new LiveListsDBStruct()
    var colNames = params[table+'Cols']
    db.transaction(function(tx){
        arr.forEach(function(subarr){
            pushArr = []
            colNames.forEach(function(colName){if(subarr[colName]){pushArr.push(subarr[colName])} else {pushArr.push('')}})
            tx.executeSql(qry,pushArr.slice(0,count),function(){completed+=1;if(completed==count){LiveListDB.log('batch completed on'+table)}},function(transaction,error){LiveListDB.log(table+error.message);/*LiveListDB.log(qry);LiveListDB.log(subarr)*/})
        })
    })
}

LiveListsDBStruct = function(){
    //structure of the websql database: bunch of tables, with the column names. WebSQL doesn't need datatypes.
    this.tables = ['Candidates','Companies','Careers','Education','Sectors','RNS','draxReferences','coDirectors'];
    this.CandidatesCols = ['Candidate_ID','Candidate_Title','Candidate_First_Name','Candidate_Middle_Name','Candidate_Surname','Candidate_Known_As','Candidate_Full_Name','Candidate_Gender','Candidate_DoB','Candidate_Region','Candidate_City','Candidate_Marital_Status','Candidate_Dependents','Candidate_B2X','Candidate_Portfolio_Size','Candidate_Number_IPOs','Candidate_Current_Plc_ID','Candidate_Phone_Number','Candidate_Email','Candidate_CHAPI_ID','Candidate_IPO_Exp','Candidate_Capacity','Candidate_Chair','Candidate_Remuneration','Candidate_Nomination','Candidate_Audit','Candidate_SID','Candidate_NED','Candidate_CSR']
    this.CompaniesCols = ['Plc_ID', 'Plc_Name','Plc_Rev_0','Plc_Rev_1','Plc_Rev_2','Plc_Rev_3','Plc_Rev_4','Plc_Rev_5','Plc_CAGR','Plc_EBITDA_0','Plc_EBITDA_1','Plc_EBITDA_2','Plc_EBITDA_3','Plc_EBITDA_4','Plc_EBITDA_5','Plc_EPS','Plc_Location','Plc_Sector','Plc_Adv_Sector','Plc_URL','Plc_BD','Plc_Company_Number']
    this.CareersCols = ['Career_Entry_ID','Candidate_ID','Career_Employer','Career_Employer_ID','Career_Employer_Ownership','Career_Role','Career_Start','Career_End']
    this.EducationCols = ['Candidate_ID','Education_Institution','Education_Subject','Education_Start','Education_End']
    this.SectorsCols = ['Candidate_ID','Sector','Adv_Sector']
    this.RNSCols = ['RNS_Item_ID','RNS_Company_ID','RNS_Headline','RNS_Date','RNS_Body']
    this.draxReferencesCols = ['draxRef_ID','Candidate_ID','Reference_Name','Reference_Company']
    this.coDirectorsCols = ['coDir_ID','Candidate_ID','Reference_Name','Reference_Company']
};

LiveListDB.createTable = function(table,cols,oncomplete){
    createStr = ''
    //creates and empties.
    createStr +='CREATE TABLE IF NOT EXISTS '+table+'('+cols.join(',')+')'
    LiveListDB.execSQL(createStr,[],function(t,r){
        LiveListDB.execSQL('DELETE FROM '+table,[],oncomplete)
    })
}

LiveListDB.dropTable = function(table,oncomplete){
    //only useful when altering the db structure - otherwise we don't drop, but only delete on an update.
    dropStr = 'DROP TABLE '+table;
    LiveListDB.execSQL(dropStr,[],oncomplete)
}

LiveListDB.addRow = function(insertStr,row,oncomplete){
    LiveListDB.execSQL(insertStr,row,oncomplete)
}

LiveListDB.buildInsertStr = function(table,cols){
    //makes a string for insertion into the Websql db. We don't need to escape as we trust the data source, and the local db can't do any damage if corrupted.
    var insertStr = 'INSERT INTO '+table+' VALUES ('
    var arr = [];cols.forEach(function(a){arr.push('?')})
    insertStr+=arr.join(', ')+')'
    return insertStr
}

LiveListDB.buildDB = function(pass){
    console.log("!!!!!!!!!!!!!!!!!!!!!!!!", pass);
    var params = new LiveListsDBStruct()
    params.tables.forEach(function(table) {
        //for each table, delete and rebuild
        var cols = params[table+'Cols']
            LiveListDB.createTable(table,cols,function(trans,results){
                LiveListDB.buildTable(table,pass)
            })
    });
}

LiveListDB.buildTable = function(table,pass){
    //run a jquery ajax call for the given table, with a basic password - this needs to be better when we go live. nocache stops accidentally cached results.
    alert(pass);
    var params = new LiveListsDBStruct()
    var req = $.ajax({
    type:"GET",
    url:"../data/getTable.php?table="+table+"&pass="+btoa('NUMIS 100'+pass)+"&nocache="+Math.random(),
    dataType:'text',
    async:true,
    isLocal:true,
    success:function(result){
            console.log('Data from '+table+'received.')
            parsed = LiveListDB.modifyObjs(table,JSON.parse(result));
            var insertStr = LiveListDB.buildInsertStr(table,params[table+'Cols']); console.log(parsed)
            LiveListDB.execSQLBatch(insertStr,parsed,doNothing,table)
        },
    error:function(error){LiveListDB.log(error)}
    });
}

LiveListDB.modifyObjs = function(table,rows){
    var retRows = []
    //fill up the columns that are empty - and if it's a virtual column, populate it
    rows.forEach(function(row){
        var params = new LiveListsDBStruct()
        var colNames = params[table+'Cols']
        colNames.forEach(
            function(param){
                if(!row[param]){row[param]=''}
                //populate sector, region
                if(param=='Plc_Sector'){row['Plc_Sector']=Fields.getSector(row['Plc_Adv_Sector'])}
                if(param=='Sector'){row['Sector']=Fields.getSector(row['Adv_Sector'])}
                if(param=='Candidate_Full_Name'){row['Candidate_Full_Name']=row['Candidate_First_Name']+' '+row['Candidate_Surname'];if(row['Candidate_Known_As']){row['Candidate_Full_Name']=row['Candidate_Known_As']}}
                if(param=='Candidate_Region'){row['Candidate_Region']=Fields.getRegion(row['Candidate_City'])}
                if(param=='Plc_Region'){row['Plc_Region']=Fields.getRegion(row['Plc_Location'])}
                if(param=='Plc_CAGR'){row['Plc_CAGR']=cagr([row.Plc_Rev_0,row.Plc_Rev_1,row.Plc_Rev_2,row.Plc_Rev_3,row.Plc_Rev_4,row.Plc_Rev_5])}
                if(param=='Candidate_IPO_Exp'){if(row['Candidate_Number_IPOs']==0){row['Candidate_IPO_Exp']='N'}else{row['Candidate_IPO_Exp']='Y'}}
            }
        )
        retRows.push(row)
    })
    return retRows
}
cagr = function(vals){
    var GRs = []
    //CAGR = mean % increase in rev over period
    for(i=0;i<vals.length-1;i++){
        if(vals[i]!=''&&vals[i+1]!=''){
            GRs.push((vals[i+1]/vals[i])-1)
        }
    }
    console.log(Math.round(avg(GRs)*1000)/10+"%");
    return Math.round(avg(GRs)*1000)/10+"%";
}
avg = function(arr){
    var ct = 0
    var sm = 0
    arr.forEach(function(a){ct+=1;sm+=a})
    return sm/ct
} 

doNothing = function(){}

LiveListDB.log = function(txt){
    if(document.getElementById('updateLog')){
        var log = document.getElementById('updateLog');
        log.innerText +=txt;
    } else{console.log(txt)}
}