update = function(){}
//page handler for update page - we don't do much except grab the user's password to pass to the updater.
update.onLoad = function(){
    if(localStorage.LLRedirectTo){localStorage.removeItem('LLRedirectTo')};
}
update.updateDB = function(){
    var passStr = $('#pass').val();
    //console.log(passStr)
    LiveListDB.buildDB(passStr);
}