search = function(){}
search.onLoad = function(){
	if(localStorage.LLRedirectTo){localStorage.removeItem('LLRedirectTo')};
    // create the checkboxes
	var tr = document.getElementById('filterContent');
    var gen = SearchCommon.createCheckboxes(tr, 'Gender',			Fields.Gender, 			null);
	var lo = SearchCommon.createCheckboxes(tr, 'Location',			Fields.Locations_Keys, 			Fields.Locations);
    var se = SearchCommon.createCheckboxes(tr, 'Sector',			Fields.Advanced_Sector_Keys, 			Fields.Advanced_Sector);
	var exp = SearchCommon.createCheckboxes(tr, 'Non-Executive Experience',			Fields.Checklist, 			null);
	var ipo = SearchCommon.createCheckboxes(tr, 'IPO Experience',			['Yes','No'], 			null);
	var cap = SearchCommon.createCheckboxes(tr, 'Capacity',			['Yes','No'], 			null);
	//set the NE Exp cluster as unset
	SearchCommon.setCheckboxGroup(6,"off");
	search.loadFromLocalStorage();

    document.getElementById('searchButton').onclick = function(e){search.onSearch();};
} 

search.onSearch = function(){
    //upon a search, get the selected checkboxes, generate SQL conditions, store them using the conditionsStore, and apply the search using the conditions array formed
    var conditions = new Array();
    var conditionStore = new Array();
    var tr = document.getElementById('filterContent');
    
	var gender_index = 3;
	var sectors_index = 5;
	var adv_sectors_index = 5;
	var locations_index = 4;
	var regions_index = 4;
	var checklist_index = 6;
	var ipo_index = 7;
	var capacity_index = 8;

	var gender = SearchCommon.getSelectedCheckboxes(tr,gender_index,false);
    if(gender!=null){
	gender = decodeGender(gender)
    if(gender.length>0){conditions.push('Candidate_Gender in (\''+gender.join('\',\'')+'\')');conditionStore.push({'field':'gender','conditions':gender})}
        else {conditions.push('Candidate_Gender =\'NOMATCH\'');conditionStore.push({'field':'gender','conditions':'NOMATCH'})}
    }
	var ipo = SearchCommon.getSelectedCheckboxes(tr,ipo_index,false);
    if(ipo!=null){
	ipo = yn(ipo)
    if(ipo.length>0){conditions.push('Candidate_IPO_Exp in (\''+ipo.join('\',\'')+'\')');conditionStore.push({'field':'ipo','conditions':unyn(ipo)})}
        else {conditions.push('Candidate_IPO_Exp =\'NOMATCH\'');conditionStore.push({'field':'ipo','conditions':'NOMATCH'})}
    }
	var capacity = SearchCommon.getSelectedCheckboxes(tr,capacity_index,false);
    if(capacity!=null){
	capacity = yn(capacity)
    if(capacity.length>0){conditions.push('Candidate_Capacity in (\''+capacity.join('\',\'')+'\')');conditionStore.push({'field':'capacity','conditions':unyn(capacity)})}
        else {conditions.push('Candidate_Capacity =\'NOMATCH\'');conditionStore.push({'field':'capacity','conditions':'NOMATCH'})}
    }
	var locations = SearchCommon.getSelectedCheckboxes(tr,locations_index,false);
    if(locations!=null){
        if(locations.length>0){conditions.push('Candidate_City in (\''+locations.join('\',\'')+'\')');conditionStore.push({'field':'locations','conditions':locations})}
        else {conditions.push('Candidate_City =\'NOMATCH\'');conditionStore.push({'field':'locations','conditions':['NOMATCH']})}
        }
	var adv_sectors = SearchCommon.getSelectedCheckboxes(tr,sectors_index,false);
    if(adv_sectors!=null){
        if(adv_sectors.length>0){conditions.push('Plc_Adv_Sector in (\''+adv_sectors.join('\',\'')+'\')');conditionStore.push({'field':'adv_sectors','conditions':adv_sectors})}
        else {conditions.push('Plc_Adv_Sector =\'NOMATCH\'');conditionStore.push({'field':'adv_sectors','conditions':['NOMATCH']})}
    }
	var checklist = SearchCommon.getSelectedCheckboxes(tr,checklist_index,false);
    if(checklist!=null){
		console.log(checklist)
		/*if(checklist.indexOf('IPO Experience')>-1){conditions.push('Candidate_IPO_Exp="Y"');conditionStore.push({field:'ipo',conditions:'y'})}
		if(checklist.indexOf('Capacity')>-1){conditions.push('Candidate_Capacity="Y"');conditionStore.push({field:'capacity',conditions:'y'})}*/
		if(checklist.indexOf('Chair')>-1){conditions.push('Candidate_Chair="Y"');conditionStore.push({field:'chair',conditions:'y'})}
		if(checklist.indexOf('Remuneration')>-1){conditions.push('Candidate_Remuneration="Y"');conditionStore.push({field:'remuneration',conditions:'y'})}
		if(checklist.indexOf('Nomination')>-1){conditions.push('Candidate_Nomination="Y"');conditionStore.push({field:'nomination',conditions:'y'})}
		if(checklist.indexOf('Audit')>-1){conditions.push('Candidate_Audit="Y"');conditionStore.push({field:'audit',conditions:'y'})}
		if(checklist.indexOf('SID')>-1){conditions.push('Candidate_SID="Y"');conditionStore.push({field:'sid',conditions:'y'})}
		if(checklist.indexOf('NED')>-1){conditions.push('Candidate_NED="Y"');conditionStore.push({field:'ned',conditions:'y'})}
		if(checklist.indexOf('CSR')>-1){conditions.push('Candidate_CSR="Y"');conditionStore.push({field:'csr',conditions:'y'})}
	}
    
	//console.log(conditionStore);
    localStorage.searchConditions = JSON.stringify(conditionStore)
    search.applySearch(conditions)
}

search.applySearch = function(conditions){
    //run the search
    console.log(conditions)
    if(conditions.length==0){LiveListDB.execSQL('SELECT * FROM candidates inner join companies on Candidates.Candidate_Current_Plc_ID=Companies.Plc_ID WHERE Candidate_ID!=""',[],function(t,r){search.displayRecords(r)})}else{
    LiveListDB.execSQL('SELECT * FROM candidates inner join companies on Candidates.Candidate_Current_Plc_ID=Companies.Plc_ID where (Candidate_ID!="" AND '+conditions.join(' AND ')+')',[],function(t,r){search.displayRecords(r)})}
}

search.loadFromLocalStorage = function(){
	//Check whether there are stored conditions. if not, just run a blank search.
	if(localStorage.searchConditions){
		var conditions = JSON.parse(localStorage.searchConditions);
		console.log(conditions)
	} else {
		search.onSearch();
		return
	};
	conditions.forEach(function(condition){
		//a condition is an object with field and condition properties - a field is the column to filter; the conditions are the options to leave selected within that field.
		switch (condition.field){
			case 'gender':
				indexToEdit = 3;
				subsect = 0
				break;
			case 'adv_sectors':
				indexToEdit = 5;
				subsect = 1
				fields = Fields.Advanced_Sector;
				break;
			case 'locations':
				indexToEdit = 4;
				subsect = 1;
				fields = Fields.Locations;
				break;
			case 'ipo':
				indexToEdit = 7;
				subsect = 0
				break;
			case 'capacity':
				indexToEdit = 8;
				subsect = 0
				break;
			default:
				indexToEdit = 0;
				subsect = 0
				break;
		}
		console.log(indexToEdit)
		console.log(subsect)
		if(indexToEdit!=0){
			var options = condition.conditions;
			console.log(options);
			SearchCommon.setCheckboxGroup(indexToEdit,"off")
			var boxesToEdit = new Array();
			for(var i=0;i<options.length;i++){
				boxesToEdit.push(indexToEdit+'_'+options[i].replace(/ /g,'_'))
			}
			if(subsect==1){
				for(i=0;i<Object.keys(fields).length;i++){
					branchName = Object.keys(fields)[i]
					branch = fields[branchName]
					selCount = 0;
					for(j=0;j<Object.keys(branch).length;j++){
						var leafName = branch[j]
						if(condition.conditions.indexOf(leafName)>-1){selCount +=1}
					}
					if(selCount== branch.length){
						console.log('_'+indexToEdit+'_'+branchName.replace(/ /g,'_'))
						document.getElementById('_'+indexToEdit+'_'+branchName.replace(/ /g,'_')).className = 'on'
					}
				}
			}
			console.log(boxesToEdit);
			SearchCommon.setSelectedCheckboxIDs(boxesToEdit);	
		} else {
			//then we're in the non-alternative checkboxes
			/*if(condition.field=='ipo'){SearchCommon.setSelectedCheckboxIDs(['1_IPO_Experience'])}
			if(condition.field=='capacity'){SearchCommon.setSelectedCheckboxIDs(['1_Capacity'])}*/
			if(condition.field=='chair'){SearchCommon.setSelectedCheckboxIDs(['6_Chair'])}
			if(condition.field=='remuneration'){SearchCommon.setSelectedCheckboxIDs(['6_Remuneration'])}
			if(condition.field=='nomination'){SearchCommon.setSelectedCheckboxIDs(['6_Nomination'])}
			if(condition.field=='audit'){SearchCommon.setSelectedCheckboxIDs(['6_Audit'])}
			if(condition.field=='sid'){SearchCommon.setSelectedCheckboxIDs(['6_SID'])}
			if(condition.field=='ned'){SearchCommon.setSelectedCheckboxIDs(['6_NED'])}
			if(condition.field=='csr'){SearchCommon.setSelectedCheckboxIDs(['6_CSR'])}
		}
	})
	search.onSearch();
}

search.pageIndex = 0;
search.pageIndexMax = 0;

search.displayRecords = function(results)
{
	var recordCount = results.rows.length;
	search.pageIndex = 1;
	search.pageIndexMax = Math.ceil(recordCount/50)
	search.displayPageOfRecords(results);
	//set behaviours of page navigation
	var pages = document.getElementById('pages');
							
	if (recordCount == 0)	FCSetInnerText(pages, 'Page 0 of 0');
	else					FCSetInnerText(pages, 'Page ' + search.pageIndex + ' of ' + search.pageIndexMax);

	// add the back and forward handlers
	document.getElementById('leftNav').onclick = function()
	{
		if (search.pageIndex > 1)
		{
			search.pageIndex--;
			
			FCSetInnerText(pages, 'Page ' + search.pageIndex + ' of ' + search.pageIndexMax);
		
			search.displayPageOfRecords(results);
		}
	}

	document.getElementById('rightNav').onclick = function()
	{
		if (search.pageIndex < search.pageIndexMax)
		{
			search.pageIndex++;
			
			FCSetInnerText(pages, 'Page ' + search.pageIndex + ' of ' + search.pageIndexMax);
		
			search.displayPageOfRecords(results);
		}
	}
}

search.displayPageOfRecords = function(results){
	
	console.log(results)
	var resultsTable = document.getElementById('resultsTable');
	// drop any existing child elements
	FCRemoveAllChildren(resultsTable);
	
	// only create the table if there are some records
	if (results.rows.length > 0)
	{
		
		var tbody = document.createElement('tbody');
		resultsTable.appendChild(tbody);
		
		// create the table header
		var header = document.createElement('tr');
		header.className = 'hdr';
		
		tbody.appendChild(header);
		
		// grab the displayed column list from persistent storage (this forcibly contains the finance director name)
		var displayedColumns = search.getDisplayColumns();
		
		// and create each of the columns in the header

		for (var i = 0; i < displayedColumns.length; i++)
		{
			var col = displayedColumns[i];
			
			if (displayedColumns.length == 1 || displayedColumns.indexOf(col) != -1)
			{
				// check if we do *not* want to display this column
				if (displayedColumns[col] != true)
				{
					search.addHeaderCol(header, search.getDisplayColumnLabels(col));

				}
			}
		}
		for (/*var i=0;i<Math.min(results.rows.length,50);i++*/var i = ((search.pageIndex - 1) * 50); i < Math.min(results.rows.length, (search.pageIndex * 50)); i++)
		{
			//console.log(i)
			var row = document.createElement('tr');
			tbody.appendChild(row);
		
			// Each result row is a standard JavaScript array indexed by column names.
			var resultRow = results.rows.item(i);
			//Create the candidate link with onclick behaviour
			var col = displayedColumns[0];
			var val = resultRow[col];
			var candidateID = resultRow.Candidate_ID;
			search.addLinkedResultCol(row,col,val,i,search.pageIndex,candidateID);

			for (var j = 1; j < displayedColumns.length; j++)
			{
				var col = displayedColumns[j];
				var val = resultRow[col];
					if (this.doNotDisplayColumns[col] != true)
					{
						search.addResultCol(row, col, val, i, search.pageIndex);
					}
			}
		}
	}
}

search.addHeaderCol = function(row, name)
{
	var col = document.createElement('td');
	FCSetInnerText(col, name);
	row.appendChild(col);
}

search.addLinkedResultCol = function(row,col,val,rowNum,pageNum,candidateID){
	var td = document.createElement('td');
	if(val){
		val = toTitleCase(val)
	}
	FCSetInnerText(td,val);
	td.onclick = function(){localStorage.CID = candidateID;window.location = 'candidate.html'};
	row.appendChild(td);
}

search.addResultCol = function(row, col, val, rowNum, pageNum)
{
	var td = document.createElement('td');
	/*if(val){
		if(col!='Most_Recent_Role'&&col!='Seeking')
		val = toTitleCase(val);
	}*/
	
	if(col=='Candidate_Full_Name'){
		//TODO: title case, linking.
		FCSetInnerText(td,val)
	} else {
		FCSetInnerText(td,val)
	}
	
	row.appendChild(td);
}

search.getDisplayColumns = function(){
    var cols = [
        'Candidate_Full_Name',
		'Candidate_Region',
		'Plc_Sector',
		'Candidate_Number_IPOs',
		'Candidate_Portfolio_Size'
    ]
    return cols
}

search.getDisplayColumnLabels = function(col){
    var colLabels = {
        'Candidate_Full_Name':'Candidate Name',
		'Candidate_Region':'Region',
		'Plc_Sector':'Sector',
		'Candidate_Number_IPOs':'# IPOs',
		'Candidate_Portfolio_Size':'Portfolio'
    }
    if(colLabels[col]){return colLabels[col]} else {return ''}
}

search.getColumnNames = function(dN){
	var dispNames = {
		'Candidate Name':'Candidate_Full_Name',
		'Region':'Candidate_Region',
		'Sector':'Plc_Sector',
		'# IPOs':'Candidate_Number_IPOs',
		'Portfolio':'Candidate_Portfolio_Size'
	}
	return(dispNames[dN])
}

search.doNotDisplayColumns = function(){
    return []
}

decodeGender = function(gender){
	var retArr = []
	gender.forEach(function(a){
		if(a == 'Male'){retArr.push('M')} 
		if(a == 'Female'){retArr.push('F')}
	})
	return retArr
}

yn = function(arr){
	var retArr = []
	if(arr.indexOf('Yes')>-1){retArr.push('Y')}
	if(arr.indexOf('No')>-1){retArr.push('N')}
	return retArr
}

unyn = function(arr){
	var retArr = []
	if(arr.indexOf('Y')>-1){retArr.push('Yes')}
	if(arr.indexOf('N')>-1){retArr.push('No')}
	return retArr
}