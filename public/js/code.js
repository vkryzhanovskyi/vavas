//Just some general quick tools
function toggle(id)
{
    //handler code for the toggle buttons
    id = '#'+id.replace(/_$/,'');
    var on = $(id).val() == 'on' ? true : false;
    $(id+'_').attr('src','../images/checkbox' + (on?'':'_') + '.gif');
    $(id).val(on?'':'on');
    return !on;
}

function toTitleCase(str)
{
    //capitalises first letter of each word.
    return str.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
}
    
function showAdvanced()
{
    //all the below is legacy code from DEC. haven't checked for dependencies, so left it in.
	$('#AdvancedInner').slideDown(600);
	$('#AdvancedContract').show();
	$('#AdvancedExpand').hide();
}

function hideAdvanced()
{
	$('#AdvancedInner').slideUp(600);
	$('#AdvancedExpand').show();
	$('#AdvancedContract').hide();
}

$(document).ready(function() {

	$('#FiltersQuestion').click(function() { $('#FiltersExplained').toggle(); });

    $('#AdvancedExpand').click(function() {
        $('#AdvancedInner').slideDown(600);
        $('#AdvancedContract').show();
        $('#AdvancedExpand').hide();
    });

    $('#AdvancedContract').click(function() {
        $('#AdvancedInner').slideUp(600);
        $('#AdvancedExpand').show();
        $('#AdvancedContract').hide();
    });

    //$('#dateFromText').click(function() { this.blur(); $('#dateFromPane').show(); });

    //$('#dateToText').click(function() { this.blur(); $('#dateToPane').show(); });

});
