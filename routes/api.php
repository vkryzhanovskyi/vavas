    <?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:api');


Route::group(['middleware' => 'api'], function () {
    Route::post('login', 'UserController@login');
    Route::get('logout', 'UserController@logout');
    Route::get('profile/{Candidate_ID?}', 'UserController@profile');
    Route::get('risersfallers', 'UserController@risersfallers');
    Route::get('rns', 'UserController@rns');
    Route::get('search', 'UserController@search');
});