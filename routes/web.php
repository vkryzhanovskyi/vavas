<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', 'HomeController@index');
Route::post('login', 'HomeController@login');
Route::get('logout', 'HomeController@logout');
Route::get('dashboard', 'HomeController@dashboard');
Route::any('search', 'HomeController@search');
Route::get('update', 'HomeController@update');
Route::get('profile/{Candidate_ID?}', 'HomeController@profile')->name('profile');

//Route::get('/', function () {
//    return view('welcome');
//});
